package de.dhbw.foodcoops.model;

import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "WarehouseItem", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "offer", type = ProductOffer.class),
            @Property(name = "amount", type = int.class)
        })
public class WarehouseItemDef {
    @ComputedProperty
    public static boolean valid(int amount, ProductOffer offer) {
        return amount > 0 && offer != null;
    }
    
    public static WarehouseItem create(int amount, ProductOffer offer) {
        WarehouseItem item = new WarehouseItem();
        
        item.setAmount(amount);
        item.setOffer(offer);
        
        return item;
    }
}
