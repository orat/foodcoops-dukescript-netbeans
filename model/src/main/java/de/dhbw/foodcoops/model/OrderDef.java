package de.dhbw.foodcoops.model;

import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "Order", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "user", type = Member.class),
            @Property(name = "shoppingCart", type = ShoppingCart.class)
        })
public class OrderDef {

    @ComputedProperty
    public static boolean valid(Member user, ShoppingCart shoppingCart) {
        return user != null
                && shoppingCart != null && shoppingCart.getItemCount() > 0;
    }

    private static boolean isNonEmptyField(String field) {
        if (field == null) {
            return false;
        }

        return !field.trim().equals("");
    }
}
