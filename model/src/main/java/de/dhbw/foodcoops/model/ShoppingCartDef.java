package de.dhbw.foodcoops.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "ShoppingCart", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "items", type = ShoppingCartItem.class, array = true)
        })
public class ShoppingCartDef {

    @ComputedProperty
    public static double totalPrice(List<ShoppingCartItem> items) {
        double totalPrice = 0;
        for (ShoppingCartItem item : items) {
            totalPrice += item.getTotalPrice();
        }

        return totalPrice;
    }

    @ComputedProperty
    public static boolean anyOutdatedOfferReferenced(List<ShoppingCartItem> items) {
        for (ShoppingCartItem item : items) {
            if (item.getOffer().isOutdated()) {
                return true;
            }
        }

        return false;
    }

    @ComputedProperty
    public static int itemCount(List<ShoppingCartItem> items) {
        return items.size();
    }

    @ComputedProperty
    public static boolean minimumOrderQuantityReached(List<ShoppingCartItem> items) {
        for (ProductOffer offer : distinctOffers(items)) {
            if (orderAmountForOffer(offer, items) < offer.getMinimumOrderQuantity()) {
                return false;
            }
        }

        return true;
    }

    private static Set<ProductOffer> distinctOffers(List<ShoppingCartItem> items) {
        Set<ProductOffer> offers = new HashSet<>();
        for (ShoppingCartItem item : items) {
            offers.add(item.getOffer());
        }

        return offers;
    }

    private static int orderAmountForOffer(ProductOffer offer, List<ShoppingCartItem> items) {
        int orderAmount = 0;

        for (ShoppingCartItem item : items) {
            if (item.getOffer().getId() == offer.getId()) {
                orderAmount += item.getAmount();
            }
        }

        return orderAmount;
    }
}
