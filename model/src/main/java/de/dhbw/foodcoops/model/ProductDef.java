package de.dhbw.foodcoops.model;
        
import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "Product", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "outdated", type = boolean.class),
            @Property(name = "name", type = String.class),
            @Property(name = "description", type = String.class)
        })
public class ProductDef {
    @ComputedProperty
    public static boolean valid(String name, String description) {
        return isNonEmptyField(name) && isNonEmptyField(description);
    }
    
    private static boolean isNonEmptyField(String field)
    {
        if(field == null) {
            return false;
        }
        
        return !field.trim().equals("");
    }
}