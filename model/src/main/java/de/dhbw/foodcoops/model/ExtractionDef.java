package de.dhbw.foodcoops.model;

import java.util.List;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "Extraction", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "user", type = Member.class),
            @Property(name = "date", type = String.class),
            @Property(name = "extractionItems", type = ExtractionItem.class, array = true),
            @Property(name = "paid", type = boolean.class)
        })
public class ExtractionDef {
    @ComputedProperty
    public static double totalPrice(List<ExtractionItem> extractionItems) {
        double totalPrice = 0;
        
        for(ExtractionItem item : extractionItems) {
            totalPrice += item.getAmount() * item.getWarehouseItem().getOffer().getPricePerUnit();
        }
        
        return totalPrice;
    }
    
    @ComputedProperty
    public static boolean valid(Member user, String date, List<ExtractionItem> extractionItems) {
        return user != null
                && isNonEmptyField(date)
                && !extractionItems.isEmpty();
    }
    
    private static boolean isNonEmptyField(String field)
    {
        if(field == null) {
            return false;
        }    
        return !field.trim().equals("");
    }
}
