package de.dhbw.foodcoops.client.android;

import android.app.Activity;
import android.content.SharedPreferences;
import de.dhbw.foodcoops.client.AdminMain;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.remote.ExtractionRestRepository;
import de.dhbw.foodcoops.database.remote.IncomingGoodsRestRepository;
import de.dhbw.foodcoops.database.remote.MemberRestRepository;
import de.dhbw.foodcoops.database.remote.OrderRestRepository;
import de.dhbw.foodcoops.database.remote.ProductOfferRestRepository;
import de.dhbw.foodcoops.database.remote.ProductRestRepository;
import de.dhbw.foodcoops.database.remote.ShoppingCartRestRepository;
import de.dhbw.foodcoops.database.remote.SupplierRestRepository;
import de.dhbw.foodcoops.database.remote.WarehouseRestRepository;
import de.dhbw.foodcoops.database.IExtractionRepository;
import de.dhbw.foodcoops.js.RestClient;
import de.dhbw.foodcoops.model.Extraction;
import de.dhbw.foodcoops.model.IncomingGoods;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Order;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.ShoppingCart;
import de.dhbw.foodcoops.model.Supplier;
import de.dhbw.foodcoops.model.WarehouseItem;

public class AndroidAdminMain extends Activity {
    private AndroidAdminMain() {
    }

    public static void main(android.content.Context context) throws Exception {
        final SharedPreferences prefs = context.getApplicationContext().getSharedPreferences(AndroidAdminMain.class.getPackage().getName(), 0);
        final AndroidServices services = new AndroidServices(prefs, context);
        final RestClient restClient = new RestClient(services);

        IRepository<Member> memberRepository = new MemberRestRepository(restClient);
        IRepository<Product> productRepository = new ProductRestRepository(restClient);
        IRepository<Order> orderRepository = new OrderRestRepository(restClient);
        IRepository<ProductOffer> productOfferRepository = new ProductOfferRestRepository(restClient);
        IRepository<Supplier> supplierRepository = new SupplierRestRepository(restClient);
        IRepository<IncomingGoods> incomingGoodsRepository = new IncomingGoodsRestRepository(restClient);
        IRepository<WarehouseItem> warehouseItemRepository = new WarehouseRestRepository(restClient);
        IExtractionRepository extractionRepository = new ExtractionRestRepository(restClient);
        IRepository<ShoppingCart> shoppingCartRepository = new ShoppingCartRestRepository(restClient);
        
        AdminMain.onPageLoad(services, memberRepository, productRepository, supplierRepository, orderRepository, productOfferRepository, incomingGoodsRepository, warehouseItemRepository, extractionRepository, shoppingCartRepository);
    }
}
