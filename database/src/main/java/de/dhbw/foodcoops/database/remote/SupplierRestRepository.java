package de.dhbw.foodcoops.database.remote;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IRestClient;
import de.dhbw.foodcoops.model.Supplier;
import java.util.List;

public class SupplierRestRepository extends RestRepositoryBase<Supplier> {

    public SupplierRestRepository(IRestClient restClient) {
        super("suppliers", Supplier.class, restClient, (m) -> m.getId());
    }
}
