package de.dhbw.foodcoops.database.remote;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IRestClient;
import de.dhbw.foodcoops.model.ProductOffer;
import java.util.List;

public class ProductOfferRestRepository extends RestRepositoryBase<ProductOffer> {
    public ProductOfferRestRepository(IRestClient restclient) {
        super("offers", ProductOffer.class, restclient, (m) -> m.getId());
    }
}
