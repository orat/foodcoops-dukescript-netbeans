package de.dhbw.foodcoops.database.remote;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IRestClient;
import de.dhbw.foodcoops.model.Product;
import java.util.List;

public class ProductRestRepository extends RestRepositoryBase<Product> {

    public ProductRestRepository(IRestClient restclient) {
        super("products", Product.class, restclient, (m) -> m.getId());
    }
}
