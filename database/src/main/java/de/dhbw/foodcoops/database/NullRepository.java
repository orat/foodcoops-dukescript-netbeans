package de.dhbw.foodcoops.database;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import java.util.ArrayList;
import java.util.List;

public class NullRepository<T> implements IRepository<T> {
    @Override
    public void add(T obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
    }

    @Override
    public void getAll(IResultHandler<List<T>> resultHandler) {
    }

    @Override
    public void remove(T obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
    }

    @Override
    public void update(T obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
    }

    @Override
    public void findById(int id, IResultHandler<T> resultHandler) {
    }
}
