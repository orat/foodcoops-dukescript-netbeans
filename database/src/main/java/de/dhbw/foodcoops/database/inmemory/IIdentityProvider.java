package de.dhbw.foodcoops.database.inmemory;

/**
 * Retrieves the unique identity for a given Object.
 */
@FunctionalInterface
public interface IIdentityProvider<T> {
    int idFor(T objectToIdentify);
}
