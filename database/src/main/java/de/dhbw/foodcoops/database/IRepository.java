package de.dhbw.foodcoops.database;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.database.resulthandling.Void;
import java.util.List;

/**
 * Enables CRUD operations on entities.
 * This is an abstraction layer to the database.
 * 
 * @param <T> Type of the managed entity
 */
public interface IRepository<T> {
    void findById(int id, IResultHandler<T> resultHandler);

    void remove(T obj, IResultHandler<Void> resultHandler);
    
    void add(T obj, IResultHandler<Void> resultHandler);
    
    void getAll(IResultHandler<List<T>> resultHandler);

    void update(T obj, IResultHandler<Void> resultHandler);
}
