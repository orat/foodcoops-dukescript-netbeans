package de.dhbw.foodcoops.database.inmemory;

public interface Identifiable {
    public int getId();
    public void setId(int id);
}
