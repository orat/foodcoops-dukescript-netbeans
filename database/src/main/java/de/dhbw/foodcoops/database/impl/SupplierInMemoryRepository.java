package de.dhbw.foodcoops.database.impl;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.inmemory.InMemoryRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.database.resulthandling.NullResultHandler;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.Supplier;
import java.util.ArrayList;
import java.util.List;

public class SupplierInMemoryRepository implements IRepository<Supplier> {

    private final IRepository<ProductOffer> offerRepository;
    private final IRepository<Supplier> supplierRepository;

    public SupplierInMemoryRepository(IRepository<ProductOffer> productOfferRepository) {
        this.offerRepository = productOfferRepository;
        this.supplierRepository = new InMemoryRepository<>((m) -> m.getId(), (id, m) -> m.setId(id));
    }

    @Override
    public void getAll(IResultHandler<List<Supplier>> resultHandler) {
        supplierRepository.getAll(new IResultHandler<List<Supplier>>() {
            @Override
            public void onSuccess(List<Supplier> allSuppliers) {
                List<Supplier> result = new ArrayList<>();
                for (Supplier supplier : allSuppliers) {
                    findById(supplier.getId(), new IResultHandler<Supplier>() {
                        @Override
                        public void onSuccess(Supplier convertedSupplier) {
                            result.add(convertedSupplier);
                        }

                        @Override
                        public void onError(Exception ex) {
                            resultHandler.onError(ex);
                        }
                    });
                }

                resultHandler.onSuccess(result);
            }

            @Override
            public void onError(Exception ex) {
                resultHandler.onError(ex);
            }
        });
    }

    @Override
    public void add(Supplier obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        for (ProductOffer productOffer : obj.getProductOffers()) {
            offerRepository.add(productOffer, new NullResultHandler<>());
        }
        
        supplierRepository.add(obj, resultHandler);
    }

    @Override
    public void remove(Supplier obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        supplierRepository.remove(obj, resultHandler);
    }

    @Override
    public void update(Supplier obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        supplierRepository.update(obj, resultHandler);
    }

    @Override
    public void findById(int id, IResultHandler<Supplier> resultHandler) {
        supplierRepository.findById(id, new IResultHandler<Supplier>() {
            @Override
            public void onSuccess(Supplier supplierResult) {
                List<ProductOffer> allOffers = new ArrayList<>();

                for (ProductOffer originalOffer : supplierResult.getProductOffers()) {
                    offerRepository.findById(originalOffer.getId(), new IResultHandler<ProductOffer>() {
                        @Override
                        public void onSuccess(ProductOffer offerResult) {
                            allOffers.add(offerResult);
                        }

                        @Override
                        public void onError(Exception ex) {
                            resultHandler.onError(ex);
                        }
                    });
                }
                
                supplierResult.getProductOffers().clear();
                supplierResult.getProductOffers().addAll(allOffers);
                resultHandler.onSuccess(supplierResult);
            }

            @Override
            public void onError(Exception ex) {
                resultHandler.onError(ex);
            }
        });
    }
}
