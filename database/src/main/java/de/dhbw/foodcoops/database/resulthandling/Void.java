package de.dhbw.foodcoops.database.resulthandling;

public final class Void {
    private Void() {}
    
    public static final Void INSTANCE = new Void();
}
