package de.dhbw.foodcoops.database.remote;

import de.dhbw.foodcoops.database.IExtractionRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IRestCallback;
import de.dhbw.foodcoops.js.IRestClient;
import de.dhbw.foodcoops.model.Extraction;
import de.dhbw.foodcoops.database.resulthandling.Void;

public class ExtractionRestRepository extends RestRepositoryBase<Extraction> implements IExtractionRepository {

    public ExtractionRestRepository(IRestClient restclient) {
        super("extractions", Extraction.class, restclient, (m) -> m.getId());
    }

    @Override
    public void pay(int id, IResultHandler<Void> resultHandler) {
        String url = createSubResourceUrl(id + "/payment");
        try {
            
        restClient.sendAsync(url, "POST", "{}", new IRestCallback()
        {
            @Override
            public void onSuccess(int statusCode, String responseText) {
                resultHandler.onSuccess(Void.INSTANCE);
            }

            @Override
            public void onError(String errorText) {
                resultHandler.onError(new Exception(errorText));
            }
        });
        } catch (Exception e) {
            System.out.println("Fuck  this shit: " + e);
        }
    }
}
