package de.dhbw.foodcoops.database.inmemory;

@FunctionalInterface
public interface IIdentitySetter<T> {
    void setId(int id, T identifiableObject);
}
