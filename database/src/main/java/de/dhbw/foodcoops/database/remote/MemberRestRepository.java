package de.dhbw.foodcoops.database.remote;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IRestClient;
import de.dhbw.foodcoops.model.Member;
import java.util.List;

public class MemberRestRepository extends RestRepositoryBase<Member> {
    public MemberRestRepository(IRestClient restclient) {
        super("members", Member.class, restclient, (m) -> m.getId());
    }
}
