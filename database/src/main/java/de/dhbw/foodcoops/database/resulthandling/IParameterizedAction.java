package de.dhbw.foodcoops.database.resulthandling;

/**
 * @param <T> Provided parameter for the operation.
 */
@FunctionalInterface
public interface IParameterizedAction<T> {
    void perform(T param);
}
