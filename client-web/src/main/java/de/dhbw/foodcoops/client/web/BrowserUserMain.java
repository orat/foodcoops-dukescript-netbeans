package de.dhbw.foodcoops.client.web;

import de.dhbw.foodcoops.client.UserMain;
import de.dhbw.foodcoops.database.CompositeRepository;
import de.dhbw.foodcoops.database.IExtractionRepository;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.impl.*;
import de.dhbw.foodcoops.database.inmemory.InMemoryRepository;
import de.dhbw.foodcoops.database.remote.ExtractionRestRepository;
import de.dhbw.foodcoops.database.remote.IncomingGoodsRestRepository;
import de.dhbw.foodcoops.database.remote.MemberRestRepository;
import de.dhbw.foodcoops.database.remote.OrderRestRepository;
import de.dhbw.foodcoops.database.remote.ProductOfferRestRepository;
import de.dhbw.foodcoops.database.remote.ProductRestRepository;
import de.dhbw.foodcoops.database.remote.ShoppingCartRestRepository;
import de.dhbw.foodcoops.database.remote.SupplierRestRepository;
import de.dhbw.foodcoops.database.remote.WarehouseRestRepository;
import de.dhbw.foodcoops.js.RestClient;
import de.dhbw.foodcoops.model.Extraction;
import de.dhbw.foodcoops.model.IncomingGoods;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Order;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.ShoppingCart;
import de.dhbw.foodcoops.model.Supplier;
import de.dhbw.foodcoops.model.WarehouseItem;

public class BrowserUserMain {
    private BrowserUserMain() {
    }

    public static void main(String... args) throws Exception {
        final Html5Services services = new Html5Services();
        final RestClient restClient = new RestClient(services);
        
        IRepository<Member> memberRepository = new MemberRestRepository(restClient);
        IRepository<Product> productRepository = new ProductRestRepository(restClient);
        IRepository<Order> orderRepository = new OrderRestRepository(restClient);
        IRepository<ProductOffer> productOfferRepository = new ProductOfferRestRepository(restClient);
        IRepository<Supplier> supplierRepository = new SupplierRestRepository(restClient);
        IRepository<IncomingGoods> incomingGoodsRepository = new IncomingGoodsRestRepository(restClient);
        IRepository<WarehouseItem> warehouseItemRepository = new WarehouseRestRepository(restClient);
        IExtractionRepository extractionRepository = new ExtractionRestRepository(restClient);
        IRepository<ShoppingCart> shoppingCartRepository = new ShoppingCartRestRepository(restClient);
        
        UserMain.onPageLoad(services, memberRepository, productRepository, supplierRepository, orderRepository, productOfferRepository, incomingGoodsRepository, warehouseItemRepository, extractionRepository, shoppingCartRepository);
    }
}
