package de.dhbw.foodcoops.client.resulthandler.decorator;

import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IParameterizedAction;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.log.ILogger;

public class ResultHandlerLogDecorator<T> implements IResultHandler<T> {

    private final ILogger logger;

    private final String successMessage;
    private final String errorMessage;
    private final IResultHandler<T> decoratedHandler;

    public ResultHandlerLogDecorator(ILogger logger, String successMessage, String errorMessage, IResultHandler<T> decoratedHandler) {
        this.logger = logger;
        this.successMessage = successMessage;
        this.errorMessage = errorMessage;
        this.decoratedHandler = decoratedHandler;
    }

    public ResultHandlerLogDecorator(ILogger logger, String successMessage, String errorMessage, IParameterizedAction<T> successAction, IParameterizedAction<Exception> failureAction) {
        this(logger, successMessage, errorMessage, new DelegateResultHandler<>(successAction, failureAction));
    }

    @Override
    public void onSuccess(T result) {
        logger.success(successMessage);
        decoratedHandler.onSuccess(result);
    }

    @Override
    public void onError(Exception ex) {
        logger.failure(errorMessage + "\nException: " + ex.toString());
        decoratedHandler.onError(ex);
    }
}
