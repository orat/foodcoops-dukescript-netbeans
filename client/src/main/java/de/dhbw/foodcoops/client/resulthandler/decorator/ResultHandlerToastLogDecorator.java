package de.dhbw.foodcoops.client.resulthandler.decorator;

import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IParameterizedAction;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.log.ILogger;

public class ResultHandlerToastLogDecorator<T> implements IResultHandler<T> {
    
    private final IResultHandler<T> handler;

    public ResultHandlerToastLogDecorator(IPlatformServices services, ILogger logger,
                                        String successMessage, String errorMessage,
                                        IParameterizedAction<T> successAction, IParameterizedAction<Exception> failureAction) {
        this(services, logger, successMessage, errorMessage, new DelegateResultHandler<>(successAction, failureAction));
    }
    
    public ResultHandlerToastLogDecorator(IPlatformServices services, ILogger logger,
                                        String successMessage, String errorMessage,
                                        IResultHandler<T> handlerToDecorate) {
        IResultHandler<T> toastHandler = new ResultHandlerToastDecorator<>(services,successMessage, errorMessage, handlerToDecorate);
        handler = new ResultHandlerLogDecorator<>(logger, successMessage, errorMessage, toastHandler);
    }

    @Override
    public void onSuccess(T result) {
        handler.onSuccess(result);
    }

    @Override
    public void onError(Exception ex) {
        handler.onError(ex);
    }

}
