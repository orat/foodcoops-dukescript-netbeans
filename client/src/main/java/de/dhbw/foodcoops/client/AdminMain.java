package de.dhbw.foodcoops.client;

import de.dhbw.foodcoops.database.IExtractionRepository;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.NullExtractionRepository;
import de.dhbw.foodcoops.database.NullRepository;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.PlatformServices;
import de.dhbw.foodcoops.model.Extraction;
import de.dhbw.foodcoops.model.IncomingGoods;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Order;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.ShoppingCart;
import de.dhbw.foodcoops.model.Supplier;
import de.dhbw.foodcoops.model.WarehouseItem;
import de.dhbw.foodcoops.viewmodel.app.admin.AdministrationAppViewModelDef;
import java.util.prefs.Preferences;
import net.java.html.boot.BrowserBuilder;

public final class AdminMain {
    private AdminMain() {
    }

    public static void main(String... args) throws Exception {
        BrowserBuilder.newBrowser().
            loadPage("pages/index.html").
            loadClass(AdminMain.class).
            invoke("onPageLoad", args).
            showAndWait();
        System.exit(0);
    }

    /**
     * Called when the page is ready.
     */
    public static void onPageLoad(
            IPlatformServices services,
            IRepository<Member> memberRepository,
            IRepository<Product> productRepository,
            IRepository<Supplier> supplierRepository,
            IRepository<Order> orderRepository,
            IRepository<ProductOffer> productOfferRepository,
            IRepository<IncomingGoods> incomingGoodsRepository,
            IRepository<WarehouseItem> warehouseItemRepository,
            IExtractionRepository extractionRepository,
            IRepository<ShoppingCart> shoppingCartRepository) throws Exception {
        services.setPreferences("app.id", "AdminApp");
        services.setPreferencesIfNotExists("api.endpoint", "http://localhost:8080/server/resources/");
        
        AdministrationAppViewModelDef.onPageLoad(services, memberRepository, productRepository, supplierRepository, orderRepository, productOfferRepository, incomingGoodsRepository, warehouseItemRepository, extractionRepository, shoppingCartRepository);
    }

    public static void onPageLoad() throws Exception {
        // don't put "common" initialization stuff here, other platforms (iOS, Android, Bck2Brwsr) may not call this method. They rather call DataModel.onPageLoad
        onPageLoad(new DesktopServices(), new NullRepository<>(), new NullRepository<>(), new NullRepository<>(), new NullRepository<>(), new NullRepository<>(), new NullRepository<>(), new NullRepository<>(), new NullExtractionRepository(), new NullRepository<>());
    }

    private static final class DesktopServices extends PlatformServices {
        @Override
        public String getPreferences(String key) {
            return Preferences.userNodeForPackage(AdminMain.class).get(key, null);
        }

        @Override
        public void setPreferences(String key, String value) {
            Preferences.userNodeForPackage(AdminMain.class).put(key, value);
        }
    }
}
