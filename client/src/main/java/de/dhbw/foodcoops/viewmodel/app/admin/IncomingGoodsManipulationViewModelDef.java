package de.dhbw.foodcoops.viewmodel.app.admin;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.DateService;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.model.IncomingGoods;
import de.dhbw.foodcoops.model.IncomingItem;
import de.dhbw.foodcoops.model.IncomingItemDef;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.app.admin.IncomingGoodsManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.OnPropertyChange;
import net.java.html.json.Property;

@Model(className = "IncomingGoodsManipulationViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "pageTitle", type = String.class),

            @Property(name = "itemToManipulate", type = IncomingGoods.class),

            @Property(name = "selectableContactPersons", type = Member.class, array = true),
            @Property(name = "selectedContactPerson", type = Member.class),

            @Property(name = "selectableProductsForAdd", type = ProductOffer.class, array = true),
            @Property(name = "selectedProductForAdd", type = ProductOffer.class),
            @Property(name = "amountOfSelectedProductToAdd", type = int.class),

            @Property(name = "selectedProducts", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class)
        })
public class IncomingGoodsManipulationViewModelDef {

    private ISaveAction<IncomingGoods> saveAction;
    private IPlatformServices services;

    public static IncomingGoodsManipulationViewModel createAddPage(IPlatformServices services, ISaveAction<IncomingGoods> saveAction, IRepository<ProductOffer> offerRepository, IRepository<Member> memberRepository) {
        IncomingGoods itemToManipulate = new IncomingGoods();

        DateService dateProvider = new DateService();
        itemToManipulate.setCreationDate(dateProvider.currentDateString());

        return createPage(services, saveAction, offerRepository, memberRepository, "Wareneingang hinzufügen", itemToManipulate);
    }

    public static IncomingGoodsManipulationViewModel createEditPage(IncomingGoods itemToManipulate, IPlatformServices services, ISaveAction<IncomingGoods> saveAction, IRepository<ProductOffer> offerRepository, IRepository<Member> memberRepository) {
        return createPage(services, saveAction, offerRepository, memberRepository, "Wareneingang bearbeiten", itemToManipulate);
    }

    private static IncomingGoodsManipulationViewModel createPage(IPlatformServices services, ISaveAction<IncomingGoods> saveAction, IRepository<ProductOffer> offerRepository, IRepository<Member> memberRepository, String pageTitle, IncomingGoods itemToManipulate) {
        IncomingGoodsManipulationViewModel vm = new IncomingGoodsManipulationViewModel();
        vm.setPageTitle(pageTitle);
        vm.setLoading(true);

        vm.setItemToManipulate(itemToManipulate);
        vm.setSelectedProducts(new ListViewModel());
        vm.initServices(services);
        vm.initSaveAction(saveAction);

        AtomicInteger numLoadingItems = new AtomicInteger(2);
        IResultHandler<List<Member>> getAllUsersResultHandler = new DelegateResultHandler<>(
                (allMembers) -> {
                    vm.getSelectableContactPersons().addAll(allMembers);
                    if (allMembers.size() > 0) {
                        vm.setSelectedContactPerson(allMembers.get(0));

                        if (0 == numLoadingItems.decrementAndGet()) {
                            vm.setLoading(false);
                        }
                    } else {
                        services.toastWarning("Keine Benutzer konnten zur Auswahl gefunden werden");
                        OnsenUITemplates.goBackOnePage();
                    }
                },
                (exception) -> {
                    services.toastWarning("Benutzer konnten nicht geladen werden: " + exception.getMessage());
                    OnsenUITemplates.goBackOnePage();
//                    vm.setLoading(false);
                });

        IResultHandler<List<ProductOffer>> getAllOffersResultHandler = new DelegateResultHandler<>(
                (productOffers) -> {
                    vm.getSelectableProductsForAdd().addAll(productOffers);
                    if (productOffers.size() > 0) {
                        vm.setSelectedProductForAdd(productOffers.get(0));

                        if (0 == numLoadingItems.decrementAndGet()) {
                            vm.setLoading(false);
                        }
                    } else {
                        services.toastWarning("Keine Produktangebote vorhanden");
                        OnsenUITemplates.goBackOnePage();
                    }
                },
                (exception) -> {
                    services.toastWarning("Produktangebote konnten nicht geladen werden: " + exception.getMessage());
                    OnsenUITemplates.goBackOnePage();
                    vm.setLoading(false);
                });

        offerRepository.getAll(getAllOffersResultHandler);
        memberRepository.getAll(getAllUsersResultHandler);

        vm.refreshProductList();

        return vm;
    }

    @ComputedProperty
    public static boolean productInputValid(int amountOfSelectedProductToAdd, ProductOffer selectedProductForAdd) {
        return amountOfSelectedProductToAdd > 0;
    }

    @ModelOperation
    public void initSaveAction(IncomingGoodsManipulationViewModel model, ISaveAction<IncomingGoods> saveAction) {
        this.saveAction = saveAction;
    }

    @ModelOperation
    public void initServices(IncomingGoodsManipulationViewModel model, IPlatformServices services) {
        this.services = services;
    }

    @Function
    public void addProduct(IncomingGoodsManipulationViewModel model) {
        ProductOffer productOffer = model.getSelectedProductForAdd();

        IncomingGoods incomingGoods = model.getItemToManipulate();
        int amount = model.getAmountOfSelectedProductToAdd();
        IncomingItem item = IncomingItemDef.create(amount, productOffer);
        incomingGoods.getIncomingItems().add(item);

        model.refreshProductList();
    }

    @ModelOperation
    public void refreshProductList(IncomingGoodsManipulationViewModel model) {
        ListViewModel productList = model.getSelectedProducts();
        productList.getItems().clear();

        List<IncomingItem> itemList = model.getItemToManipulate().getIncomingItems();
        for (IncomingItem incomingItem : itemList) {
            Product product = incomingItem.getOffer().getProduct();
            String description = "Menge: " + incomingItem.getAmount();
            ListItemViewModel productItem = new ListItemViewModel(product.getName(), description);

            ItemAction deleteAction = new ItemAction("md-delete");
            deleteAction.init(() -> {
                services.confirmByUser("Produkt wirklich aus dem Wareneingang löschen?", () -> {
                    itemList.remove(incomingItem);
                    productList.getItems().remove(productItem);
                });
            });
            productItem.getActions().add(deleteAction);

            productList.getItems().add(productItem);
        }
    }

    @OnPropertyChange("selectedContactPerson")
    public void cotactPersonChanged(IncomingGoodsManipulationViewModel model) {
        model.getItemToManipulate().setContactPerson(model.getSelectedContactPerson());
    }

    @Function
    public void save(IncomingGoodsManipulationViewModel model) {
        IncomingGoods incomingGoods = model.getItemToManipulate();

        if (!incomingGoods.isValid()) {
            services.toastWarning("Please fill all form fields");
            return;
        }

        saveAction.save(incomingGoods);
    }
}
