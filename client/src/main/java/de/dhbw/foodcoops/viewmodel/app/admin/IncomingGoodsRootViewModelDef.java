package de.dhbw.foodcoops.viewmodel.app.admin;

import de.dhbw.foodcoops.client.resulthandler.decorator.ResultHandlerToastLogDecorator;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.log.ILogger;
import de.dhbw.foodcoops.model.IncomingGoods;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.app.admin.IncomingGoodsManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.app.admin.IncomingGoodsRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "IncomingGoodsRootViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "incomingGoodsManipulation", type = IncomingGoodsManipulationViewModel.class),
            @Property(name = "incomingGoodsDetails", type = IncomingGoods.class),
            @Property(name = "incomingGoodsList", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class)
        })
public class IncomingGoodsRootViewModelDef {

    private IPlatformServices services;
    private ILogger logger;
    private IRepository<IncomingGoods> incomingGoodsRepository;
    private IRepository<ProductOffer> productOfferRepository;
    private IRepository<Member> memberRepository;

    public static IncomingGoodsRootViewModel create(IPlatformServices services,
            ILogger logger,
            IRepository<IncomingGoods> incomingGoodsRepository,
            IRepository<ProductOffer> productOfferRepository,
            IRepository<Member> memberRepository) {
        IncomingGoodsRootViewModel vm = new IncomingGoodsRootViewModel();
        vm.setLoading(true);

        vm.initServices(services);
        vm.initLogger(logger);
        vm.initIncomingGoodsRepository(incomingGoodsRepository);
        vm.initProductOfferRepository(productOfferRepository);
        vm.initMemberRepository(memberRepository);

        ListViewModel incomingGoodsList = new ListViewModel();
        vm.setIncomingGoodsList(incomingGoodsList);
        vm.setIncomingGoodsDetails(new IncomingGoods());

        return vm;
    }

    @ModelOperation
    public void initIncomingGoodsRepository(IncomingGoodsRootViewModel model, IRepository<IncomingGoods> repository) {
        this.incomingGoodsRepository = repository;
    }

    @ModelOperation
    public void initProductOfferRepository(IncomingGoodsRootViewModel model, IRepository<ProductOffer> repository) {
        this.productOfferRepository = repository;
    }

    @ModelOperation
    public void initMemberRepository(IncomingGoodsRootViewModel model, IRepository<Member> repository) {
        this.memberRepository = repository;
    }

    @ModelOperation
    public void initServices(IncomingGoodsRootViewModel model, IPlatformServices services) {
        this.services = services;

        model.getIncomingGoodsList().initServices(services);
    }

    @ModelOperation
    public void initLogger(IncomingGoodsRootViewModel model, ILogger logger) {
        this.logger = logger;
    }

    @Function
    public void navigateToAddPage(IncomingGoodsRootViewModel model) {
        ISaveAction<IncomingGoods> saveAction = (newIncomingGoods) -> {
            IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> saveResultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Wareneingang erfolgreich gespeichert", "Fehler beim Speichern des Wareneingangs",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            incomingGoodsRepository.add(newIncomingGoods, saveResultHandler);
        };

        IncomingGoodsManipulationViewModel manipulationVM = IncomingGoodsManipulationViewModelDef.createAddPage(services, saveAction, productOfferRepository, memberRepository);
        model.setIncomingGoodsManipulation(manipulationVM);

        OnsenUITemplates.switchPage("administrationApp/incomingGoods/manipulate.html");
    }

    private static void refreshAndGoToHome(IncomingGoodsRootViewModel model, IPlatformServices services) {
        model.refreshIncomingGoodsList();
        OnsenUITemplates.goBackOnePage();
    }

    @ModelOperation
    public void navigateToEditPage(IncomingGoodsRootViewModel model, IncomingGoods incomingGoodsToEdit) {
        ISaveAction<IncomingGoods> saveAction = (editedIncomingGoods) -> {
            IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Wareneingang erfolgreich gespeichert", "Fehler beim Speichern des Wareneingangs",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            incomingGoodsRepository.update(editedIncomingGoods, resultHandler);
            refreshAndGoToHome(model, services);
        };

        IncomingGoodsManipulationViewModel manipulationVM = IncomingGoodsManipulationViewModelDef.createEditPage(incomingGoodsToEdit.clone(), services, saveAction, productOfferRepository, memberRepository);
        model.setIncomingGoodsManipulation(manipulationVM);

        OnsenUITemplates.switchPage("administrationApp/incomingGoods/manipulate.html");
    }

    @Function
    @ModelOperation
    public void refreshIncomingGoodsList(IncomingGoodsRootViewModel model) {
        model.setLoading(true);

        IResultHandler<List<IncomingGoods>> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                "Liste der Wareneingänge erfolgreich geladen", "Fehler beim Laden der Wareneingangs-Liste",
                (result) -> {
                    model.getIncomingGoodsList().getItems().clear();
                    for (IncomingGoods incomingGoods : result) {
                        ListItemViewModel listItem = createListItemFor(model, incomingGoodsRepository, services, logger, incomingGoods);
                        model.getIncomingGoodsList().getItems().add(listItem);
                    }

                    model.setLoading(false);
                },
                (exception) -> {
                    model.setLoading(false);
                });

        incomingGoodsRepository.getAll(resultHandler);
    }

    @ModelOperation
    public void navigateToDetailsPage(IncomingGoodsRootViewModel model, IncomingGoods incomingGoods) {
        model.setIncomingGoodsDetails(incomingGoods);
        OnsenUITemplates.switchPage("administrationApp/incomingGoods/details.html");
    }

    private static ListItemViewModel createListItemFor(IncomingGoodsRootViewModel model, IRepository<IncomingGoods> repository, IPlatformServices services, ILogger logger, IncomingGoods incomingGoods) {
        String itemTitle = incomingGoods.getId() + ": " + incomingGoods.getContactPerson().getDisplayName();
        String itemDescription = incomingGoods.getIncomingItems().size() + " Produkt(e) | Erstellungsdatum: " + incomingGoods.getCreationDate();
        ListItemViewModel listItem = new ListItemViewModel(itemTitle, itemDescription);

        ItemAction editAction = new ItemAction("md-edit");
        editAction.init(() -> model.navigateToEditPage(incomingGoods));
        listItem.getActions().add(editAction);

        ItemAction deleteAction = new ItemAction("md-delete");
        deleteAction.init(() -> {
            String deleteMessage = "Wareneingang wirklich löschen?\nAchtung: Dies hat keine Auswirkung auf den Lagerbestand. Dieser muss manuell angepasst werden.";
            services.confirmByUser(deleteMessage, () -> {
                IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                        "Wareneingang wurde erfolgreich gelöscht", "Fehler beim Löschen des Wareneingangs",
                        (result) -> model.refreshIncomingGoodsList(),
                        (exception) -> model.refreshIncomingGoodsList());

                repository.remove(incomingGoods, resultHandler);
            });
        });
        listItem.getActions().add(deleteAction);

        listItem.initDetailsAction(() -> model.navigateToDetailsPage(incomingGoods));

        return listItem;
    }
}
