package de.dhbw.foodcoops.viewmodel.app.admin;

import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ShoppingCart;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.app.admin.ProductManipulationViewModel;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "ProductManipulationViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "pageTitle", type = String.class),
            @Property(name = "itemToManipulate", type = Product.class)
        })
public class ProductManipulationViewModelDef {
    private ISaveAction<Product> saveAction;
    private IPlatformServices services;

    public static ProductManipulationViewModel createAddPage(IPlatformServices services, ISaveAction<Product> saveAction) {
        return createPage(services, saveAction, "Produkt hinzufügen", new Product());
    }

    public static ProductManipulationViewModel createEditPage(Product itemToManipulate, IPlatformServices services, ISaveAction<Product> saveAction) {
        return createPage(services, saveAction, "Produkt bearbeiten", itemToManipulate);
    }

    private static ProductManipulationViewModel createPage(IPlatformServices services, ISaveAction<Product> saveAction, String pageTitle, Product itemToManipulate) {
        ProductManipulationViewModel vm = new ProductManipulationViewModel();
        vm.setPageTitle(pageTitle);

        vm.setItemToManipulate(itemToManipulate);
        vm.initServices(services);
        vm.initSaveAction(saveAction);

        return vm;
    }

    @ModelOperation
    public void initSaveAction(ProductManipulationViewModel model, ISaveAction<Product> saveAction) {
        this.saveAction = saveAction;
    }
    
    @ModelOperation
    public void initServices(ProductManipulationViewModel model, IPlatformServices services) {
        this.services = services;
    }
    
    @ComputedProperty
    public static boolean valid(Product itemToManipulate)
    {
        return itemToManipulate.isValid();
    }

    @Function
    public void save(ProductManipulationViewModel model) {
        Product product = model.getItemToManipulate();

        if (!product.isValid()) {
            services.toastWarning("Please fill all form fields");
            return;
        }

        saveAction.save(product);
    }
}
