package de.dhbw.foodcoops.viewmodel.shared.settings;

import de.dhbw.foodcoops.js.IPlatformServices;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.OnPropertyChange;
import net.java.html.json.Property;

@Model(className = "CredentialsViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "userName", type = String.class),
            @Property(name = "password", type = String.class)})
public class CredentialsViewModelDef {
    private IPlatformServices services;
    
    public static CredentialsViewModel create(IPlatformServices services)
    {
        CredentialsViewModel model = new CredentialsViewModel();
        
        model.initServices(services);
        
        String username = services.getPreferences("authorization.user.name", "?");
        String password = services.getPreferences("authorization.user.password", "?");
        model.setUserName(username);
        model.setPassword(password);
        
        return model;
    }
    
    @ModelOperation
    public void initServices(CredentialsViewModel model, IPlatformServices services)
    {
        this.services = services;
    }
    
    @OnPropertyChange("password")
    public void onPasswordChange(CredentialsViewModel model)
    {
        services.setPreferences("authorization.user.password", model.getPassword());
    }
    
    @OnPropertyChange("userName")
    public void onNameChange(CredentialsViewModel model)
    {
        services.setPreferences("authorization.user.name", model.getUserName());
    }
}
