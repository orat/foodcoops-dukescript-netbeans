package de.dhbw.foodcoops.viewmodel.shared;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.WarehouseItem;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.shared.WarehouseItemManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.shared.WarehouseItemManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.OnPropertyChange;
import net.java.html.json.Property;

@Model(className = "WarehouseItemManipulationViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "pageTitle", type = String.class),
            @Property(name = "itemToManipulate", type = WarehouseItem.class),
            @Property(name = "selectableProductsForAdd", type = ProductOffer.class, array = true),
            @Property(name = "selectedProductForAdd", type = ProductOffer.class),
            @Property(name = "amountOfSelectedProductToAdd", type = int.class),
            @Property(name = "selectedProducts", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class)
        })
public class WarehouseItemManipulationViewModelDef {

    private ISaveAction<WarehouseItem> saveAction;
    private IPlatformServices services;

    public static WarehouseItemManipulationViewModel createAddPage(IPlatformServices services, ISaveAction<WarehouseItem> saveAction, IRepository<ProductOffer> offerRepository) {
        WarehouseItemManipulationViewModel vm = createPage(services, saveAction, offerRepository, "Lagerelement hinzufügen", new WarehouseItem());
        
        vm.setLoading(true);
        IResultHandler<List<ProductOffer>> getAllResultHandler = new DelegateResultHandler<>(
                (productOffers) -> {
                    vm.getSelectableProductsForAdd().addAll(productOffers);

                    if (productOffers.size() > 0) {
                        vm.setSelectedProductForAdd(productOffers.get(0));
                        vm.setLoading(false);
                    } else {
                        services.toastWarning("Keine Produktangebote vorhanden");
                        OnsenUITemplates.goBackOnePage();
                    }
                },
                (exception) -> {
                    services.toastWarning("Produktangebote konnten nicht geladen werden: " + exception.getMessage());
                    OnsenUITemplates.goBackOnePage();
                    vm.setLoading(false);
                });
        offerRepository.getAll(getAllResultHandler);
        
        return vm;
    }

    public static WarehouseItemManipulationViewModel createEditPage(WarehouseItem itemToManipulate, IPlatformServices services, ISaveAction<WarehouseItem> saveAction, IRepository<ProductOffer> offerRepository) {
        WarehouseItemManipulationViewModel vm = createPage(services, saveAction, offerRepository, "Lagerelement bearbeiten", itemToManipulate);
        
        vm.setSelectedProductForAdd(itemToManipulate.getOffer());
        vm.getSelectableProductsForAdd().add(itemToManipulate.getOffer());
        
        return vm;
    }

    private static WarehouseItemManipulationViewModel createPage(IPlatformServices services, ISaveAction<WarehouseItem> saveAction, IRepository<ProductOffer> offerRepository, String pageTitle, WarehouseItem itemToManipulate) {
        WarehouseItemManipulationViewModel vm = new WarehouseItemManipulationViewModel();
        vm.setPageTitle(pageTitle);

        vm.setItemToManipulate(itemToManipulate);
        vm.setSelectedProducts(new ListViewModel());
        vm.initServices(services);
        vm.initSaveAction(saveAction);

        return vm;
    }

    @ComputedProperty
    public static boolean productInputValid(int amountOfSelectedProductToAdd, ProductOffer selectedProductForAdd) {
        return amountOfSelectedProductToAdd >= selectedProductForAdd.getMinimumOrderQuantity();
    }

    @ModelOperation
    public void initSaveAction(WarehouseItemManipulationViewModel model, ISaveAction<WarehouseItem> saveAction) {
        this.saveAction = saveAction;
    }

    @ModelOperation
    public void initServices(WarehouseItemManipulationViewModel model, IPlatformServices services) {
        this.services = services;
    }

    @ComputedProperty
    public static boolean warehouseItemInputValid(int amountOfSelectedProductToAdd, ProductOffer selectedProductForAdd) {
        return amountOfSelectedProductToAdd > 0 && selectedProductForAdd != null;
    }

    @OnPropertyChange("selectedProductForAdd")
    public void onSelectedProductChange(WarehouseItemManipulationViewModel model) {
        ProductOffer productOffer = model.getSelectedProductForAdd();
        if (productOffer == null || !productOffer.isValid()) {
            services.toastWarning("No (valid) product selected ....");
            return;
        }

        WarehouseItem warehouseItem = model.getItemToManipulate();
        warehouseItem.setOffer(productOffer);
    }

    @OnPropertyChange("amountOfSelectedProductToAdd")
    public void onAmountChange(WarehouseItemManipulationViewModel model) {
        WarehouseItem warehouseItem = model.getItemToManipulate();
        warehouseItem.setAmount(model.getAmountOfSelectedProductToAdd());
    }

    @Function
    public void save(WarehouseItemManipulationViewModel model) {
        WarehouseItem warehouseItem = model.getItemToManipulate();

        if (!warehouseItem.isValid()) {
            services.toastWarning("Please fill all form fields");
            return;
        }
        
        saveAction.save(warehouseItem);
    }
}
