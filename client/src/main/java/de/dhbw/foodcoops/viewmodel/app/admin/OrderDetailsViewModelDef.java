package de.dhbw.foodcoops.viewmodel.app.admin;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.model.Order;
import de.dhbw.foodcoops.model.ShoppingCartItem;
import de.dhbw.foodcoops.model.Supplier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "OrderDetailsViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),

            @Property(name = "order", type = Order.class),
            @Property(name = "userName", type = String.class),
            @Property(name = "selectableSuppliers", type = Supplier.class, array = true),
            @Property(name = "selectedSupplier", type = Supplier.class)
        })
public class OrderDetailsViewModelDef {

    public static OrderDetailsViewModel create(Order order, IRepository<Supplier> supplierRepository, IPlatformServices services) {
        OrderDetailsViewModel vm = new OrderDetailsViewModel();
        vm.setOrder(order);
        vm.setUserName(order.getUser().getDisplayName());

        Set<Integer> supplierIds = new HashSet<>();

        for (ShoppingCartItem item : order.getShoppingCart().getItems()) {
            supplierIds.add(item.getOffer().getSupplierId());
        }

        loadSuppliers(vm, supplierRepository, services, supplierIds);

        return vm;
    }

    private static void loadSuppliers(OrderDetailsViewModel vm, IRepository<Supplier> supplierRepository, IPlatformServices services, Set<Integer> ids) {
        vm.setLoading(true);
        
        if(ids.isEmpty()) {
            vm.setLoading(false);
            return;
        }

        IResultHandler<List<Supplier>> getAllHandler = new DelegateResultHandler<>(
                (allSuppliers) -> {
                    Set<Supplier> selectableSuppliers = new HashSet<>();

                    try {
                        for (int id : ids) {
                            Supplier supplier = supplierForId(id, allSuppliers);
                            selectableSuppliers.add(supplier);
                        }

                        vm.getSelectableSuppliers().clear();
                        vm.getSelectableSuppliers().addAll(selectableSuppliers);

                        Iterator<Supplier> supplierIterator = selectableSuppliers.iterator();
                        if (supplierIterator.hasNext()) {
                            vm.setSelectedSupplier(supplierIterator.next());
                            vm.setLoading(false);
                        }
                        else {
                            services.toastWarning("Bestellung konnte keinen Lieferanten zugeordnet werden.");
                        }
                    } catch (Exception ex) {
                        services.toastWarning("Lieferanten-Details konnten nicht geladen werden");
                    }
                },
                (exception) -> {
                    services.toastWarning("Lieferanten-Details konnten nicht geladen werden");
                });

        supplierRepository.getAll(getAllHandler);
    }

    private static Supplier supplierForId(int id, List<Supplier> suppliers) {
        for (Supplier s : suppliers) {
            if (s.getId() == id) {
                return s;
            }
        }

        throw new IndexOutOfBoundsException("No Supplier found for id: " + id);
    }

    @ComputedProperty
    public static double totalPrice(Order order, Supplier selectedSupplier) {
        double totalPrice = 0;

        for (ShoppingCartItem item : order.getShoppingCart().getItems()) {
            if (item.getOffer().getSupplierId() == selectedSupplier.getId()) {
                totalPrice += item.getTotalPrice();
            }
        }

        return totalPrice;
    }

    @ComputedProperty
    public static String supplierName(Supplier selectedSupplier) {
        return selectedSupplier.getName();
    }

    @ComputedProperty
    public static List<ShoppingCartItem> visualizedItems(Order order, Supplier selectedSupplier) {
        List<ShoppingCartItem> items = new ArrayList<>();

        for (ShoppingCartItem item : order.getShoppingCart().getItems()) {
            if (item.getOffer().getSupplierId() == selectedSupplier.getId()) {
                items.add(item);
            }
        }

        return items;
    }
}
