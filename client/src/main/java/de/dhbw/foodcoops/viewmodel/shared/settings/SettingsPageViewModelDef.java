package de.dhbw.foodcoops.viewmodel.shared.settings;

import de.dhbw.foodcoops.js.IPlatformServices;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.OnPropertyChange;
import net.java.html.json.Property;

@Model(className = "SettingsPageViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "serverEndpoint", type = String.class),
            @Property(name = "toastDurationMilliseconds", type = long.class),
            @Property(name = "credentials", type = CredentialsViewModel.class),})
public class SettingsPageViewModelDef {

    private IPlatformServices services;

    public static SettingsPageViewModel create(IPlatformServices services) {
        SettingsPageViewModel model = new SettingsPageViewModel();

        model.initServices(services);

        CredentialsViewModel credentialVM = CredentialsViewModelDef.create(services);
        model.setCredentials(credentialVM);

        String serverEndpoint = services.getPreferences("api.endpoint", "");
        String toastDuration = services.getPreferences("app.toast.duration", "1500");
        model.setServerEndpoint(serverEndpoint);
        try {
            model.setToastDurationMilliseconds(Long.valueOf(toastDuration));
        } catch(Exception ex) {
            model.setToastDurationMilliseconds(1500);
        }

        return model;
    }

    @ModelOperation
    public void initServices(SettingsPageViewModel model, IPlatformServices services) {
        this.services = services;
    }

    @OnPropertyChange("serverEndpoint")
    public void endpointChanged(SettingsPageViewModel model) {
        services.setPreferences("api.endpoint", model.getServerEndpoint());
    }

    @OnPropertyChange("toastDurationMilliseconds")
    public void toastDurationChanged(SettingsPageViewModel model) {
        services.setPreferences("app.toast.duration", String.valueOf(model.getToastDurationMilliseconds()));
    }
}
