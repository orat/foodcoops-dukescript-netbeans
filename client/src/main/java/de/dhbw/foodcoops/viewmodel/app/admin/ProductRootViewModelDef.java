package de.dhbw.foodcoops.viewmodel.app.admin;

import de.dhbw.foodcoops.client.resulthandler.decorator.ResultHandlerToastLogDecorator;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.database.resulthandling.Void;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.log.ILogger;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.viewmodel.app.admin.ProductManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.app.admin.ProductRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "ProductRootViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "productManipulation", type = ProductManipulationViewModel.class),
            @Property(name = "productDetails", type = Product.class),
            @Property(name = "productList", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class)
        })
public class ProductRootViewModelDef {

    private IRepository<Product> repository;
    private IPlatformServices services;
    private ILogger logger;

    public static ProductRootViewModel create(IPlatformServices services, ILogger logger, IRepository<Product> productRepository) {
        ProductRootViewModel model = new ProductRootViewModel();

        model.setLoading(true);
        model.initRepository(productRepository);
        model.initServices(services);
        model.initLogger(logger);

        ListViewModel productList = new ListViewModel();
        model.setProductList(productList);

        return model;
    }

    @ModelOperation
    public void initRepository(ProductRootViewModel model, IRepository<Product> repository) {
        this.repository = repository;
    }

    @ModelOperation
    public void initServices(ProductRootViewModel model, IPlatformServices services) {
        this.services = services;

        model.getProductList().initServices(services);
    }

    @ModelOperation
    public void initLogger(ProductRootViewModel model, ILogger logger) {
        this.logger = logger;
    }

    @Function
    public void navigateToAddPage(ProductRootViewModel model) {
        ISaveAction<Product> saveAction = (newProduct) -> {
            IResultHandler<Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Produkt erfolgreich gespeichert", "Fehler beim Speichern des Produkts",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            repository.add(newProduct, resultHandler);

        };

        ProductManipulationViewModel manipulationVM = ProductManipulationViewModelDef.createAddPage(services, saveAction);
        model.setProductManipulation(manipulationVM);

        OnsenUITemplates.switchPage("administrationApp/product/manipulate.html");
    }

    private static void refreshAndGoToHome(ProductRootViewModel model, IPlatformServices services) {
        model.refreshProductList();
        OnsenUITemplates.goBackOnePage();
    }

    @ModelOperation
    public void navigateToEditPage(ProductRootViewModel model, Product productToEdit) {
        ISaveAction<Product> saveAction = (editedProduct) -> {
            IResultHandler<Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Produkt erfolgreich gespeichert", "Fehler beim Speichern des Produkts",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            repository.update(editedProduct, resultHandler);
            refreshAndGoToHome(model, services);
        };

        ProductManipulationViewModel manipulationVM = ProductManipulationViewModelDef.createEditPage(productToEdit.clone(), services, saveAction);
        model.setProductManipulation(manipulationVM);

        OnsenUITemplates.switchPage("administrationApp/product/manipulate.html");
    }

    @Function
    @ModelOperation
    public void refreshProductList(ProductRootViewModel model) {
        model.setLoading(true);

        IResultHandler<List<Product>> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                "Produkt-Liste erfolgreich geladen", "Fehler beim Laden der Produkt-Liste",
                (result) -> {
                    model.getProductList().getItems().clear();
                    for (Product product : result) {
                        if (!product.isOutdated()) {
                            ListItemViewModel listItem = createListItemFor(model, repository, services, logger, product);
                            model.getProductList().addItem(listItem);
                        }
                    }

                    model.setLoading(false);
                },
                (exception) -> {
                    model.setLoading(false);
                });

        repository.getAll(resultHandler);
    }

    @ModelOperation
    public void navigateToDetailsPage(ProductRootViewModel model, Product product) {
        model.setProductDetails(product);
        OnsenUITemplates.switchPage("administrationApp/product/details.html");
    }

    private static ListItemViewModel createListItemFor(ProductRootViewModel model, IRepository<Product> repository, IPlatformServices services, ILogger logger, Product product) {
        ListItemViewModel listItem = new ListItemViewModel(product.getId() + ": " + product.getName(), product.getDescription());

        ItemAction editAction = new ItemAction("md-edit");
        editAction.init(() -> model.navigateToEditPage(product));
        listItem.getActions().add(editAction);

        ItemAction deleteAction = new ItemAction("md-delete");
        deleteAction.init(() -> {
            services.confirmByUser("Produkt wirklich löschen?", () -> {
                IResultHandler<Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                        "Produkt erfolgreich gelöscht", "Fehler beim Löschen des Produkts",
                        (result) -> model.refreshProductList(),
                        (exception) -> model.refreshProductList());

                repository.remove(product, resultHandler);
            });
        });
        listItem.getActions().add(deleteAction);

        listItem.initDetailsAction(() -> model.navigateToDetailsPage(product));

        return listItem;
    }
}
