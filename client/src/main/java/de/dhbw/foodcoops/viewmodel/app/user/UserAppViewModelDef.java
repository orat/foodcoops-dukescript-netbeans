package de.dhbw.foodcoops.viewmodel.app.user;

import de.dhbw.foodcoops.database.IExtractionRepository;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.log.ILogger;
import de.dhbw.foodcoops.log.VisualLogger;
import de.dhbw.foodcoops.model.Extraction;
import de.dhbw.foodcoops.model.IncomingGoods;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Order;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.ShoppingCart;
import de.dhbw.foodcoops.model.Supplier;
import de.dhbw.foodcoops.model.WarehouseItem;
import de.dhbw.foodcoops.viewmodel.app.admin.AdministrationAppViewModel;
import de.dhbw.foodcoops.viewmodel.shared.ShoppingCartRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.ShoppingCartRootViewModelDef;
import de.dhbw.foodcoops.viewmodel.shared.ExtractionRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.ExtractionRootViewModelDef;
import de.dhbw.foodcoops.viewmodel.shared.WarehouseItemRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.WarehouseItemRootViewModelDef;
import de.dhbw.foodcoops.viewmodel.shared.log.LogViewModel;
import de.dhbw.foodcoops.viewmodel.shared.settings.SettingsPageViewModel;
import de.dhbw.foodcoops.viewmodel.shared.settings.SettingsPageViewModelDef;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "UserAppViewModel", targetId = "", instance = true,
        properties = {
            @Property(name = "extractionRoot", type = de.dhbw.foodcoops.viewmodel.shared.ExtractionRootViewModel.class),
            @Property(name = "shoppingCartRoot", type = de.dhbw.foodcoops.viewmodel.shared.ShoppingCartRootViewModel.class),
            @Property(name = "warehouseItemRoot", type = de.dhbw.foodcoops.viewmodel.shared.WarehouseItemRootViewModel.class),
            
            @Property(name = "log", type = de.dhbw.foodcoops.viewmodel.shared.log.LogViewModel.class),
            @Property(name = "settings", type = de.dhbw.foodcoops.viewmodel.shared.settings.SettingsPageViewModel.class)
        })
public class UserAppViewModelDef {

    public static void onPageLoad(
            IPlatformServices services,
            IRepository<Member> memberRepository,
            IRepository<Product> productRepository,
            IRepository<Supplier> supplierRepository,
            IRepository<Order> orderRepository,
            IRepository<ProductOffer> productOfferRepository,
            IRepository<IncomingGoods> incomingGoodsRepository,
            IRepository<WarehouseItem> warehouseItemRepository,
            IExtractionRepository extractionRepository,
            IRepository<ShoppingCart> shoppingCartRepository) {
        UserAppViewModel userAppViewModel = new UserAppViewModel();

        SettingsPageViewModel settingsPageVM = SettingsPageViewModelDef.create(services);
        userAppViewModel.setSettings(settingsPageVM);

        LogViewModel logVM = new LogViewModel();
        ILogger logger = new VisualLogger(logVM);
        userAppViewModel.setLog(logVM);
        services.decorateSystemLogger((msg) -> logger.log("Console-Log", msg),
                (msg) -> logger.log("Console-Info", msg),
                (msg) -> logger.log("Console-Warning", msg),
                (msg) -> logger.log("Console-Error", msg));

        ShoppingCartRootViewModel shoppingCartRootVM = ShoppingCartRootViewModelDef.create(services, logger, shoppingCartRepository, productOfferRepository, memberRepository, orderRepository);
        ExtractionRootViewModel extractionRootVM = ExtractionRootViewModelDef.create(services, logger, extractionRepository, warehouseItemRepository, memberRepository);
        WarehouseItemRootViewModel warehouseItemRootVM = WarehouseItemRootViewModelDef.create(services, logger, warehouseItemRepository, productOfferRepository);

        userAppViewModel.setShoppingCartRoot(shoppingCartRootVM);
        userAppViewModel.setExtractionRoot(extractionRootVM);
        userAppViewModel.setWarehouseItemRoot(warehouseItemRootVM);

        userAppViewModel.applyBindings();
        OnsenUITemplates.registerListener();
        
        userAppViewModel.navigateUserApp();
    }

    @Function
    public void navigateSettings() {
        OnsenUITemplates.resetToPage("shared/settings.html");
        OnsenUITemplates.closeSidebar();
    }

    @Function
    @ModelOperation
    public void navigateUserApp(UserAppViewModel model) {
        OnsenUITemplates.resetToPage("userApp/tabbar-userapp.html");
        
        model.getShoppingCartRoot().refreshShoppingCartList();
        model.getExtractionRoot().refreshExtractionList();
        model.getWarehouseItemRoot().refreshWarehouseItemList();
        
        OnsenUITemplates.closeSidebar();
    }
    
    @Function
    public void navigateHelpPage(UserAppViewModel model)
    {
        OnsenUITemplates.switchPage("userApp/help.html");
        OnsenUITemplates.closeSidebar();
    }
    
    @Function
    public void toggleSidebar()
    {
        OnsenUITemplates.toggleSidebar();
    }
}
