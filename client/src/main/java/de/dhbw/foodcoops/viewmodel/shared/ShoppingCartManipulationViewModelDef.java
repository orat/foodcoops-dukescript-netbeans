package de.dhbw.foodcoops.viewmodel.shared;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.ShoppingCart;
import de.dhbw.foodcoops.model.ShoppingCartItem;
import de.dhbw.foodcoops.model.ShoppingCartItemDef;
import de.dhbw.foodcoops.viewmodel.shared.ShoppingCartManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "ShoppingCartManipulationViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "pageTitle", type = String.class),

            @Property(name = "itemToManipulate", type = ShoppingCart.class),

            @Property(name = "selectableUsers", type = Member.class, array = true),
            @Property(name = "selectedUser", type = Member.class),

            @Property(name = "selectableProductsForAdd", type = ProductOffer.class, array = true),
            @Property(name = "selectedProductForAdd", type = ProductOffer.class),
            @Property(name = "selectedProducts", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class),
            @Property(name = "amountOfSelectedProductToAdd", type = int.class)

        })
public class ShoppingCartManipulationViewModelDef {

    private ISaveAction<ShoppingCart> saveAction;
    private IPlatformServices services;

    public static ShoppingCartManipulationViewModel createAddPage(IPlatformServices services, ISaveAction<ShoppingCart> saveAction, IRepository<ProductOffer> offerRepository, IRepository<Member> memberRepository) {
        return createPage(services, saveAction, offerRepository, memberRepository, "Einkaufswagen hinzufügen", new ShoppingCart());
    }

    public static ShoppingCartManipulationViewModel createEditPage(ShoppingCart itemToManipulate, IPlatformServices services, ISaveAction<ShoppingCart> saveAction, IRepository<ProductOffer> offerRepository, IRepository<Member> memberRepository) {
        return createPage(services, saveAction, offerRepository, memberRepository, "Einkaufswagen bearbeiten", itemToManipulate);
    }

    private static ShoppingCartManipulationViewModel createPage(IPlatformServices services, ISaveAction<ShoppingCart> saveAction, IRepository<ProductOffer> offerRepository, IRepository<Member> memberRepository, String pageTitle, ShoppingCart itemToManipulate) {
        ShoppingCartManipulationViewModel vm = new ShoppingCartManipulationViewModel();
        vm.setLoading(true);
        vm.setPageTitle(pageTitle);

        vm.setItemToManipulate(itemToManipulate);
        vm.setSelectedProducts(new ListViewModel());
        vm.initServices(services);
        vm.initSaveAction(saveAction);

        AtomicInteger numLoadingItems = new AtomicInteger(2);
        IResultHandler<List<Member>> getAllUsersResultHandler = new DelegateResultHandler<>(
                (members) -> {
                    vm.getSelectableUsers().addAll(members);
                    if (members.size() > 0) {
                        vm.setSelectedUser(members.get(0));

                        if (0 == numLoadingItems.decrementAndGet()) {
                            vm.setLoading(false);
                        }
                    } else {
                        services.toastWarning("Keine Benutzer konnten zur Auswahl gefunden werden");
                        OnsenUITemplates.goBackOnePage();
                    }
                },
                (exception) -> {
                    services.toastWarning("Benutzer konnten nicht geladen werden: " + exception.getMessage());
                    OnsenUITemplates.goBackOnePage();
//                    vm.setLoading(false);
                });

        IResultHandler<List<ProductOffer>> getAllOffersResultHandler = new DelegateResultHandler<>(
                (productOffers) -> {
                    vm.getSelectableProductsForAdd().addAll(productOffers);
                    if (productOffers.size() > 0) {
                        vm.setSelectedProductForAdd(productOffers.get(0));

                        if (0 == numLoadingItems.decrementAndGet()) {
                            vm.setLoading(false);
                        }
                    } else {
                        services.toastWarning("Keine Produktangebote konnten zur Auswahl gefunden werden");
                        OnsenUITemplates.goBackOnePage();
                    }
                },
                (exception) -> {
                    services.toastWarning("Produktangebote konnten nicht geladen werden: " + exception.getMessage());
                    OnsenUITemplates.goBackOnePage();
//                    vm.setLoading(false);
                });
        offerRepository.getAll(getAllOffersResultHandler);
        memberRepository.getAll(getAllUsersResultHandler);

        vm.refreshItemList();

        return vm;
    }

    @ModelOperation
    public void initSaveAction(ShoppingCartManipulationViewModel model, ISaveAction<ShoppingCart> saveAction) {
        this.saveAction = saveAction;
    }

    @ModelOperation
    public void initServices(ShoppingCartManipulationViewModel model, IPlatformServices services) {
        this.services = services;
    }

    @Function
    public void addProduct(ShoppingCartManipulationViewModel model) {
        ProductOffer productOffer = model.getSelectedProductForAdd();
        if (productOffer == null || !productOffer.isValid()) {
            services.toastWarning("No (valid) product selected ....");
            return;
        }

        ShoppingCart shoppingCart = model.getItemToManipulate();
        int amount = model.getAmountOfSelectedProductToAdd();
        ShoppingCartItem productItem = ShoppingCartItemDef.create(amount, productOffer, model.getSelectedUser());
        shoppingCart.getItems().add(productItem);

        model.refreshItemList();
    }

    @ModelOperation
    public void refreshItemList(ShoppingCartManipulationViewModel model) {
        ListViewModel productList = model.getSelectedProducts();
        productList.getItems().clear();

        List<ShoppingCartItem> itemList = model.getItemToManipulate().getItems();
        for (ShoppingCartItem cartItem : itemList) {
            Product product = cartItem.getOffer().getProduct();
            String description = "Mitglied: " + cartItem.getUser().getDisplayName()
                    + " | Menge: " + cartItem.getAmount()
                    + " | Gesamtpreis: " + cartItem.getTotalPrice() + " €"
                    + " | Bestellziel: " + calculateMinimumOrderQuantityGoalProgress(model.getItemToManipulate(), cartItem);
            ListItemViewModel productItem = new ListItemViewModel(product.getName(), description);

            ItemAction deleteAction = new ItemAction("md-delete");
            deleteAction.init(() -> {
                services.confirmByUser("Eintrag wirklich aus dem Einkaufswagen löschen?", () -> {
                    itemList.remove(cartItem);
                    productList.getItems().remove(productItem);
                });
            });
            productItem.getActions().add(deleteAction);

            productList.getItems().add(productItem);
        }
    }

    private static String calculateMinimumOrderQuantityGoalProgress(ShoppingCart cart, ShoppingCartItem forItem) {
        int actual = 0;
        int minimumOrderQuantity = forItem.getOffer().getMinimumOrderQuantity();
        int offerId = forItem.getOffer().getId();

        for (ShoppingCartItem item : cart.getItems()) {
            if (item.getOffer().getId() == offerId) {
                actual += item.getAmount();
            }
        }

        return actual + "/" + minimumOrderQuantity;
    }

    @ComputedProperty
    public static String productInputErrors(int amountOfSelectedProductToAdd, Member selectedUser, ProductOffer selectedProductForAdd) {
        if (selectedProductForAdd == null) {
            return "Wähle ein Produkt zum Hinzufügen aus.";
        }
        if (selectedUser == null) {
            return "Wähle ein Mitglied zum Hinzufügen aus.";
        }

        if (amountOfSelectedProductToAdd <= 0) {
            return "Gebe eine gültige Menge für den Eintrag ein (größer oder gleich 1)";
        }

        return null;
    }

    @Function
    public void save(ShoppingCartManipulationViewModel model) {
        ShoppingCart shoppingCart = model.getItemToManipulate();

        saveAction.save(shoppingCart);
    }
}
