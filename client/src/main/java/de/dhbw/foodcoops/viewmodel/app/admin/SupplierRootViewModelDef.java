package de.dhbw.foodcoops.viewmodel.app.admin;

import de.dhbw.foodcoops.client.resulthandler.decorator.ResultHandlerToastLogDecorator;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.database.resulthandling.Void;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.log.ILogger;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.Supplier;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.app.admin.SupplierManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.app.admin.SupplierRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "SupplierRootViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "supplierManipulation", type = SupplierManipulationViewModel.class),
            @Property(name = "supplierDetails", type = Supplier.class),
            @Property(name = "supplierList", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class)
        })
public class SupplierRootViewModelDef {

    private IPlatformServices services;
    private ILogger logger;
    private IRepository<Supplier> supplierRepository;
    private IRepository<ProductOffer> productOfferRepository;
    private IRepository<Product> productRepository;

    public static SupplierRootViewModel create(IPlatformServices services, ILogger logger,
            IRepository<Supplier> supplierRepository, IRepository<ProductOffer> productOfferRepository,
            IRepository<Product> productRepository) {
        SupplierRootViewModel model = new SupplierRootViewModel();

        model.setLoading(true);
        model.initServices(services);
        model.initLogger(logger);
        model.initSupplierRepository(supplierRepository);
        model.initProductOfferRepository(productOfferRepository);
        model.initProductRepository(productRepository);

        model.setSupplierDetails(new Supplier());

        ListViewModel supplierList = new ListViewModel();
        model.setSupplierList(supplierList);

        return model;
    }

    @ModelOperation
    public void initSupplierRepository(SupplierRootViewModel model, IRepository<Supplier> repository) {
        this.supplierRepository = repository;
    }

    @ModelOperation
    public void initProductOfferRepository(SupplierRootViewModel model, IRepository<ProductOffer> repository) {
        this.productOfferRepository = repository;
    }

    @ModelOperation
    public void initProductRepository(SupplierRootViewModel model, IRepository<Product> repository) {
        this.productRepository = repository;
    }

    @ModelOperation
    public void initServices(SupplierRootViewModel model, IPlatformServices services) {
        this.services = services;

        model.getSupplierList().initServices(services);
    }

    @ModelOperation
    public void initLogger(SupplierRootViewModel model, ILogger logger) {
        this.logger = logger;
    }

    @Function
    public void navigateToAddPage(SupplierRootViewModel model) {
        ISaveAction<Supplier> saveAction = (newSupplier) -> {
            IResultHandler<Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Lieferant erfolgreich gespeichert", "Fehler beim Speichern des Lieferanten",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            supplierRepository.add(newSupplier, resultHandler);
        };

        SupplierManipulationViewModel manipulationVM = SupplierManipulationViewModelDef.createAddPage(services, saveAction, productRepository);
        model.setSupplierManipulation(manipulationVM);

        OnsenUITemplates.switchPage("administrationApp/supplier/manipulate.html");
    }

    private static void refreshAndGoToHome(SupplierRootViewModel model, IPlatformServices services) {
        model.refreshSupplierList();
        OnsenUITemplates.goBackOnePage();
    }

    @ModelOperation
    public void navigateToEditPage(SupplierRootViewModel model, Supplier supplierToEdit) {
        ISaveAction<Supplier> saveAction = (editedSupplier) -> {
            IResultHandler<Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Lieferant erfolgreich gespeichert", "Fehler beim Speichern des Lieferanten",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            supplierRepository.update(editedSupplier, resultHandler);
        };

        SupplierManipulationViewModel manipulationVM = SupplierManipulationViewModelDef.createEditPage(supplierToEdit.clone(), services, saveAction, productRepository);
        model.setSupplierManipulation(manipulationVM);

        OnsenUITemplates.switchPage("administrationApp/supplier/manipulate.html");
    }

    @Function
    @ModelOperation
    public void refreshSupplierList(SupplierRootViewModel model) {
        model.setLoading(true);

        IResultHandler<List<Supplier>> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                "Lieferanten-Liste erfolgreich geladen", "Fehler beim Laden der Lieferanten-Liste",
                (result) -> {
                    model.getSupplierList().getItems().clear();
                    for (Supplier supplier : result) {
                        if (!supplier.isOutdated()) {
                            ListItemViewModel listItem = createListItemFor(model, supplierRepository, services, logger, supplier);
                            model.getSupplierList().getItems().add(listItem);
                        }
                    }

                    model.setLoading(false);
                },
                (exception) -> {
                    model.setLoading(false);
                });

        supplierRepository.getAll(resultHandler);
    }

    @ModelOperation
    public void navigateToDetailsPage(SupplierRootViewModel model, Supplier supplier) {
        model.setSupplierDetails(supplier);
        OnsenUITemplates.switchPage("administrationApp/supplier/details.html");
    }

    private static ListItemViewModel createListItemFor(SupplierRootViewModel model, IRepository<Supplier> repository, IPlatformServices services, ILogger logger, Supplier supplier) {
        String itemTitle = supplier.getId() + ": " + supplier.getName();
        String itemDescription = supplier.getProductOffers().size() + " Angebot(e) | Ansprechpartner: " + supplier.getContactPerson();
        ListItemViewModel listItem = new ListItemViewModel(itemTitle, itemDescription);

        ItemAction editAction = new ItemAction("md-edit");
        editAction.init(() -> model.navigateToEditPage(supplier));
        listItem.getActions().add(editAction);

        ItemAction deleteAction = new ItemAction("md-delete");
        deleteAction.init(() -> {
            services.confirmByUser("Lieferant wirklich löschen?", () -> {
                IResultHandler<Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                        "Lieferant erfolgreich gelöscht", "Fehler beim Löschen des Lieferanten",
                        (result) -> model.refreshSupplierList(),
                        (exception) -> model.refreshSupplierList());

                repository.remove(supplier, resultHandler);
            });
        });
        listItem.getActions().add(deleteAction);

        listItem.initDetailsAction(() -> model.navigateToDetailsPage(supplier));

        return listItem;
    }
}
