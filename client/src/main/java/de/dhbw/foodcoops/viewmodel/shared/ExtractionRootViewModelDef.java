package de.dhbw.foodcoops.viewmodel.shared;

import de.dhbw.foodcoops.client.resulthandler.decorator.ResultHandlerToastLogDecorator;
import de.dhbw.foodcoops.database.IExtractionRepository;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.IRestCallback;
import de.dhbw.foodcoops.js.IRestClient;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.log.ILogger;
import de.dhbw.foodcoops.model.Extraction;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.WarehouseItem;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "ExtractionRootViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "extractionManipulation", type = ExtractionManipulationViewModel.class),
            @Property(name = "extractionDetails", type = Extraction.class),
            @Property(name = "extractionList", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class),
            @Property(name = "adminApp", type = boolean.class)
        })
public class ExtractionRootViewModelDef {

    private IPlatformServices services;
    private ILogger logger;
    private IExtractionRepository extractionRepository;
    private IRepository<WarehouseItem> warehouseItemRepository;
    private IRepository<Member> memberRepository;

    public static ExtractionRootViewModel create(IPlatformServices services, ILogger logger,
            IExtractionRepository extractionRepository,
            IRepository<WarehouseItem> warehouseItemRepository,
            IRepository<Member> memberRepository) {
        ExtractionRootViewModel vm = new ExtractionRootViewModel();
        vm.setLoading(true);

        vm.initServices(services);
        vm.initLogger(logger);
        vm.initExtractionRepository(extractionRepository);
        vm.initWarehouseItemRepository(warehouseItemRepository);
        vm.initMemberRepository(memberRepository);

        boolean isAdminApp = services.getPreferences("app.id", "").equals("AdminApp");
        vm.setAdminApp(isAdminApp);

        ListViewModel extractionList = new ListViewModel();
        vm.setExtractionList(extractionList);
        vm.setExtractionDetails(new Extraction());

        return vm;
    }

    @ModelOperation
    public void initExtractionRepository(ExtractionRootViewModel model, IExtractionRepository repository) {
        this.extractionRepository = repository;
    }

    @ModelOperation
    public void initWarehouseItemRepository(ExtractionRootViewModel model, IRepository<WarehouseItem> repository) {
        this.warehouseItemRepository = repository;
    }

    @ModelOperation
    public void initMemberRepository(ExtractionRootViewModel model, IRepository<Member> repository) {
        this.memberRepository = repository;
    }

    @ModelOperation
    public void initServices(ExtractionRootViewModel model, IPlatformServices services) {
        this.services = services;

        model.getExtractionList().initServices(services);
    }

    @ModelOperation
    public void initLogger(ExtractionRootViewModel model, ILogger logger) {
        this.logger = logger;
    }

    @Function
    public void navigateToAddPage(ExtractionRootViewModel model) {
        ISaveAction<Extraction> saveAction = (newExtraction) -> {
            IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> saveResultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Abholung erfolgreich gespeichert", "Fehler beim Speichern der Abholung",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            extractionRepository.add(newExtraction, saveResultHandler);
        };

        ExtractionManipulationViewModel manipulationVM = ExtractionManipulationViewModelDef.createAddPage(services, saveAction, warehouseItemRepository, memberRepository);
        model.setExtractionManipulation(manipulationVM);

        OnsenUITemplates.switchPage("shared/extraction/manipulate.html");
    }

    private static void refreshAndGoToHome(ExtractionRootViewModel model, IPlatformServices services) {
        model.refreshExtractionList();
        OnsenUITemplates.goBackOnePage();
    }

    @ModelOperation
    public void navigateToEditPage(ExtractionRootViewModel model, Extraction extractionToEdit) {
        ISaveAction<Extraction> saveAction = (editedExtraction) -> {
            IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Abholung erfolgreich gespeichert", "Fehler beim Speichern der Abholung",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            extractionRepository.update(editedExtraction, resultHandler);
            refreshAndGoToHome(model, services);
        };

        ExtractionManipulationViewModel manipulationVM = ExtractionManipulationViewModelDef.createEditPage(extractionToEdit.clone(), services, saveAction, warehouseItemRepository, memberRepository);
        model.setExtractionManipulation(manipulationVM);

        OnsenUITemplates.switchPage("shared/extraction/manipulate.html");
    }

    @Function
    @ModelOperation
    public void refreshExtractionList(ExtractionRootViewModel model) {
        model.setLoading(true);

        IResultHandler<List<Extraction>> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                "Abhol-Liste wurde erfolgreich geladen", "Fehler beim Laden der Abhol-Liste",
                (result) -> {
                    model.getExtractionList().getItems().clear();
                    for (Extraction extraction : result) {
                        ListItemViewModel listItem = createListItemFor(model, extractionRepository, services, logger, extraction);
                        model.getExtractionList().getItems().add(listItem);
                    }

                    model.setLoading(false);
                },
                (exception) -> {
                    model.setLoading(false);
                });

        extractionRepository.getAll(resultHandler);
    }

    @ModelOperation
    public void navigateToDetailsPage(ExtractionRootViewModel model, Extraction extraction) {
        model.setExtractionDetails(extraction);
        OnsenUITemplates.switchPage("shared/extraction/details.html");
    }

    private static ListItemViewModel createListItemFor(ExtractionRootViewModel model, IExtractionRepository repository, IPlatformServices services, ILogger logger, Extraction extraction) {
        String itemTitle = extraction.getId() + ": " + extraction.getUser().getDisplayName();
        String paidSymbol = extraction.isPaid() ? "✓" : "X";
        String itemDescription = "Bezahlt: " + paidSymbol + " | " + extraction.getExtractionItems().size() + " Lagerprodukt(e)";

        ListItemViewModel listItem = new ListItemViewModel(itemTitle, itemDescription);
        listItem.initDetailsAction(() -> model.navigateToDetailsPage(extraction));

        if (!extraction.isPaid()) {
            String userName = services.getPreferences("authorization.user.name", "--");
            boolean extractionBelongsToCurrentUser = userName.equals(extraction.getUser().getUserName());

            if (model.isAdminApp() || extractionBelongsToCurrentUser) {
                ItemAction paymentAction = createPaymentAction(model, services, logger, repository, extraction);
                listItem.getActions().add(paymentAction);
            }
        }

        if (model.isAdminApp()) {
            ItemAction editAction = new ItemAction("md-edit");
            editAction.init(() -> model.navigateToEditPage(extraction));
            listItem.getActions().add(editAction);

            ItemAction deleteAction = new ItemAction("md-delete");
            deleteAction.init(() -> {
                String deleteQuestion = "Wirklich löschen?\n"
                        + "Das Löschen wirkt sich nicht auf den Lagerbestand aus. Dieser muss manuell korrigiert werden.";
                
                services.confirmByUser(deleteQuestion, () -> {
                    IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                            "Abholung erfolgreich gelöscht", "Fehler beim Löschen der Abholung",
                            (result) -> model.refreshExtractionList(),
                            (exception) -> model.refreshExtractionList());

                    repository.remove(extraction, resultHandler);
                });
            });
            listItem.getActions().add(deleteAction);
        }

        return listItem;
    }

    private static ItemAction createPaymentAction(ExtractionRootViewModel model, IPlatformServices services, ILogger logger, IExtractionRepository repository, Extraction extraction) {
        ItemAction payAction = new ItemAction("md-money");
        payAction.init(() -> {
            services.confirmByUser("Abholung als bezahlt markieren?", () -> {
                IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                        "Abholung wurde erfolgreich bezahlt", "Fehler bei der Bezahlung",
                        (success) -> model.refreshExtractionList(),
                        (exception) -> model.refreshExtractionList());

                repository.pay(extraction.getId(), resultHandler);
            });
        });

        return payAction;
    }
}
