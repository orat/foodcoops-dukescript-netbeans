package de.dhbw.foodcoops.viewmodel.shared;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.DateService;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.model.Extraction;
import de.dhbw.foodcoops.model.ExtractionItem;
import de.dhbw.foodcoops.model.ExtractionItemDef;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.WarehouseItem;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.OnPropertyChange;
import net.java.html.json.Property;

@Model(className = "ExtractionManipulationViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "pageTitle", type = String.class),

            @Property(name = "itemToManipulate", type = Extraction.class),

            @Property(name = "selectableUsers", type = Member.class, array = true),
            @Property(name = "selectedUser", type = Member.class),

            @Property(name = "selectableWarehouseItemsToAdd", type = WarehouseItem.class, array = true),
            @Property(name = "selectedWarehouseItemToAdd", type = WarehouseItem.class),
            @Property(name = "amountOfSelectedWarehouseItemToAdd", type = int.class),

            @Property(name = "selectedWarehouseItems", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class)
        })
public class ExtractionManipulationViewModelDef {

    private ISaveAction<Extraction> saveAction;
    private IPlatformServices services;

    public static ExtractionManipulationViewModel createAddPage(IPlatformServices services, ISaveAction<Extraction> saveAction, IRepository<WarehouseItem> warehouseItemRepository, IRepository<Member> memberRepository) {
        return createPage(services, saveAction, warehouseItemRepository, memberRepository, "Abholung hinzufügen", new Extraction());
    }

    public static ExtractionManipulationViewModel createEditPage(Extraction itemToManipulate, IPlatformServices services, ISaveAction<Extraction> saveAction, IRepository<WarehouseItem> warehouseItemRepository, IRepository<Member> memberRepository) {
        return createPage(services, saveAction, warehouseItemRepository, memberRepository, "Abholung bearbeiten", itemToManipulate);
    }

    private static ExtractionManipulationViewModel createPage(IPlatformServices services, ISaveAction<Extraction> saveAction, IRepository<WarehouseItem> warehouseItemRepository, IRepository<Member> memberRepository, String pageTitle, Extraction itemToManipulate) {
        itemToManipulate.setDate(new DateService().currentDateString());

        ExtractionManipulationViewModel vm = new ExtractionManipulationViewModel();
        vm.setPageTitle(pageTitle);
        vm.setLoading(true);

        vm.setItemToManipulate(itemToManipulate);
        vm.setSelectedWarehouseItems(new ListViewModel());
        vm.initServices(services);
        vm.initSaveAction(saveAction);

        AtomicInteger numLoadingItems = new AtomicInteger(2);
        IResultHandler<List<Member>> getAllUsersResultHandler = new DelegateResultHandler<>(
                (members) -> {
                    vm.getSelectableUsers().addAll(members);
                    if (members.size() > 0) {
                        vm.setSelectedUser(members.get(0));

                        if (0 == numLoadingItems.decrementAndGet()) {
                            vm.refreshProductList();
                            vm.setLoading(false);
                        }
                    } else {
                        services.toastWarning("Keine Benutzer konnten zur Auswahl gefunden werden");
                        OnsenUITemplates.goBackOnePage();
                    }
                },
                (exception) -> {
                    services.toastWarning("Benutzer konnten nicht geladen werden: " + exception.getMessage());
                    OnsenUITemplates.goBackOnePage();
//                    vm.setLoading(false);
                });

        IResultHandler<List<WarehouseItem>> getAllResultHandler = new DelegateResultHandler<>(
                (warehouseItems) -> {
                    vm.getSelectableWarehouseItemsToAdd().addAll(warehouseItems);
                    if (warehouseItems.size() > 0) {
                        vm.setSelectedWarehouseItemToAdd(warehouseItems.get(0));

                        if (0 == numLoadingItems.decrementAndGet()) {
                            vm.refreshProductList();
                            vm.setLoading(false);
                        }
                    } else {
                        services.toastWarning("Keine Lagerelemente vorhanden");
                        OnsenUITemplates.goBackOnePage();
                    }
                },
                (exception) -> {
                    services.toastWarning("Lagerelemente konnten nicht geladen werden: " + exception.getMessage());
                    OnsenUITemplates.goBackOnePage();
                    vm.setLoading(false);
                });

        warehouseItemRepository.getAll(getAllResultHandler);
        memberRepository.getAll(getAllUsersResultHandler);

        return vm;
    }

    @ComputedProperty
    public static String extractionItemInputErrors(int amountOfSelectedWarehouseItemToAdd, WarehouseItem selectedWarehouseItemToAdd, Extraction itemToManipulate) {
        if (selectedWarehouseItemToAdd == null) {
            return "Wähle ein Element zur Entnahme aus";
        }

        if (extractionContainsWarehouseItem(itemToManipulate, selectedWarehouseItemToAdd)) {
            return "Dieses Element befindet sich bereits in der Liste";
        }
        if (amountOfSelectedWarehouseItemToAdd <= 0) {
            return "Die Abhol-Menge muss größer 0 sein";
        }
        if (amountOfSelectedWarehouseItemToAdd > selectedWarehouseItemToAdd.getAmount()) {
            return "Es können nicht mehr Elemente aus dem Lager entnommen werden, als vorhanden. Noch auf Lager: " + selectedWarehouseItemToAdd.getAmount();
        }

        return null;
    }

    @ComputedProperty
    public static boolean valid(Extraction itemToManipulate) {
        return itemToManipulate != null
                && itemToManipulate.getExtractionItems().size() > 0;
    }

    private static boolean extractionContainsWarehouseItem(Extraction extraction, WarehouseItem item) {
        for (ExtractionItem extrationItem : extraction.getExtractionItems()) {
            if (extrationItem.getWarehouseItem().getId() == item.getId()) {
                return true;
            }
        }

        return false;
    }

    @ModelOperation
    public void initSaveAction(ExtractionManipulationViewModel model, ISaveAction<Extraction> saveAction) {
        this.saveAction = saveAction;
    }

    @ModelOperation
    public void initServices(ExtractionManipulationViewModel model, IPlatformServices services) {
        this.services = services;
    }

    @Function
    public void addItem(ExtractionManipulationViewModel model) {
        WarehouseItem warehouseItem = model.getSelectedWarehouseItemToAdd();
        if (warehouseItem == null || !warehouseItem.isValid()) {
            services.toastWarning("No (valid) product selected ....");
            return;
        }

        Extraction extraction = model.getItemToManipulate();
        int amount = model.getAmountOfSelectedWarehouseItemToAdd();
        ExtractionItem extractionItem = ExtractionItemDef.create(amount, warehouseItem);
        extraction.getExtractionItems().add(extractionItem);

        model.refreshProductList();
    }

    @ModelOperation
    public void refreshProductList(ExtractionManipulationViewModel model) {
        ListViewModel extractionList = model.getSelectedWarehouseItems();
        extractionList.getItems().clear();

        List<ExtractionItem> itemList = model.getItemToManipulate().getExtractionItems();
        for (ExtractionItem extractionItem : itemList) {
            Product product = extractionItem.getWarehouseItem().getOffer().getProduct();
            String description = "Menge: " + extractionItem.getAmount();
            ListItemViewModel extractionLVM = new ListItemViewModel(product.getName(), description);

            ItemAction deleteAction = new ItemAction("md-delete");
            deleteAction.init(() -> {
                services.confirmByUser("Produkt wirklich aus der Entnahme löschen?", () -> {
                    itemList.remove(extractionItem);
                    extractionList.getItems().remove(extractionLVM);
                });
            });
            extractionLVM.getActions().add(deleteAction);

            extractionList.getItems().add(extractionLVM);
        }
    }

    @OnPropertyChange("selectedUser")
    public void selectedUserChanged(ExtractionManipulationViewModel model) {
        model.getItemToManipulate().setUser(model.getSelectedUser());
    }

    @ComputedProperty
    public static String totalPrice(Extraction itemToManipulate) {
        double totalPrice = itemToManipulate.getTotalPrice();
        return totalPrice + " €";
    }

    @Function
    public void save(ExtractionManipulationViewModel model) {
        Extraction order = model.getItemToManipulate();

        if (!order.isValid()) {
            services.toastWarning("Please fill all form fields");
            return;
        }

        saveAction.save(order);
    }
}
