package de.dhbw.foodcoops.server;

import de.dhbw.foodcoops.model.Member;
import java.util.Base64;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AuthorizationUtils {
    
    public static boolean isMember(String authorizationHeader)
    {
        return (null != memberForCredentials(authorizationHeader));
    }
    
    public static boolean isAdmin(String authorizationHeader)
    {
        Member member = memberForCredentials(authorizationHeader);
        
        return member != null && member.isAdmin();
    }

    public static Member memberForCredentials(String authorizationHeader) {
        EntityManagerFactory emfactory = null;
        EntityManager entitymanager = null;
        try {
            emfactory = Persistence.createEntityManagerFactory("FoodCoops-ServerDB");
            entitymanager = emfactory.createEntityManager();

            String encodedUsernamePassword = authorizationHeader.substring("Basic ".length()).trim();
            String usernamePassword = new String(Base64.getDecoder().decode(encodedUsernamePassword));
            int seperatorIndex = usernamePassword.indexOf(':');

            String name = usernamePassword.substring(0, seperatorIndex);
            String password = usernamePassword.substring(seperatorIndex + 1);
            List<Member> resultList = entitymanager.createQuery("Select c from Member c where c.userName=:name AND c.password=:password")
                    .setParameter("name", name)
                    .setParameter("password", password).getResultList();
            
            if(resultList.size() == 1)
            {
                Member result = resultList.get(0);
                if(!result.isOutdated()) {
                    return result;
                }
            }
            
            return null;
        } catch (Exception ex) {
            System.out.println("Failed authorization");
            ex.printStackTrace();
            return null;
        } finally {
            if (entitymanager != null) {
                entitymanager.close();
            }
            if (emfactory != null) {
                emfactory.close();
            }
        }
    }
}
