package de.dhbw.foodcoops.server.resources;

import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.server.AuthorizationUtils;
import static de.dhbw.foodcoops.server.ResponseFactory.*;
import java.util.stream.Stream;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("products")
public class ProductResource extends BasicResource<Product> {

    public ProductResource() {
        super("Product", Product.class);
    }

    @OPTIONS
    public Response preflightRequest_Collection() {
        System.out.println("Preflight ProductCollection");
        return okResponse();
    }

    @OPTIONS
    @Path("/{id}")
    public Response preflightRequest_Resource() {
        System.out.println("Preflight products/{id}");
        return okResponse();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllResponse(@HeaderParam("Authorization") String authorizationHeader) {
        if (!AuthorizationUtils.isMember(authorizationHeader)) {
            return forbiddenStatus("Ungültige Anmeldeinformationen");
        }

        return super.getAllResponse();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isMember(authorizationHeader)) {
            return forbiddenStatus("Ungültige Anmeldeinformationen");
        }
        return super.findById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addItem(@HeaderParam("Authorization") String authorizationHeader,
            Product itemToAdd) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("This resource can only be modified by admins.");
        }

        return super.add(itemToAdd);
    }

    @DELETE
    @Path("/{id}")
    public Response removeItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("This resource can only be modified by admins.");
        }

        try {
            return performOperation(entityManager -> {
                Stream<ProductOffer> referencingOffers = entityManager.createQuery("Select o From ProductOffer o Where o.product.id=" + id).getResultStream();
                boolean isReferencedByCurrentOffers = referencingOffers.anyMatch(offer -> !offer.isOutdated());
                
                if(isReferencedByCurrentOffers) {
                    return conflictResponse("Produkt wird von aktuellen Angeboten referenziert.");
                }
                
                entityManager.getTransaction().begin();
                Product product = entityManager.find(Product.class, id);
                product.setOutdated(true);
                
                entityManager.merge(product);
                
                entityManager.getTransaction().commit();
                
                return okResponse();
            });
        } catch (Exception ex) {
            return serverErrorResponse(ex.getMessage());
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id,
            String newValues) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("This resource can only be modified by admins.");
        }

        return super.updateItem(newValues);
    }
}
