package de.dhbw.foodcoops.server;

import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("resources")
public class JAXRSConfiguration extends Application {
    
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(de.dhbw.foodcoops.server.resources.ExtractionResource.class);
        resources.add(de.dhbw.foodcoops.server.resources.IncomingGoodsResource.class);
        resources.add(de.dhbw.foodcoops.server.resources.MemberResource.class);
        resources.add(de.dhbw.foodcoops.server.resources.OrderResource.class);
        resources.add(de.dhbw.foodcoops.server.resources.ProductOfferResource.class);
        resources.add(de.dhbw.foodcoops.server.resources.ProductResource.class);
        resources.add(de.dhbw.foodcoops.server.resources.ShoppingCartResource.class);
        resources.add(de.dhbw.foodcoops.server.resources.SupplierResource.class);
        resources.add(de.dhbw.foodcoops.server.resources.WarehouseItemResource.class);
    }
}
