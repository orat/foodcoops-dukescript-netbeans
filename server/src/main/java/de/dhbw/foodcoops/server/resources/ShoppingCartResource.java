package de.dhbw.foodcoops.server.resources;

import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.ShoppingCart;
import de.dhbw.foodcoops.model.ShoppingCartItem;
import de.dhbw.foodcoops.server.AuthorizationUtils;
import static de.dhbw.foodcoops.server.ResponseFactory.*;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("shoppingcarts")
public class ShoppingCartResource extends BasicResource<ShoppingCart> {

    public ShoppingCartResource() {
        super("ShoppingCart", ShoppingCart.class);
    }

    @OPTIONS
    public Response preflightRequest_Collection() {
        return super.preflightRequest_Collection();
    }

    @OPTIONS
    @Path("/{id}")
    public Response preflightRequest_Resource() {
        return super.preflightRequest_Resource();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@HeaderParam("FC-App") String appId,
            @HeaderParam("Authorization") String authorizationHeader) {
        Member member = AuthorizationUtils.memberForCredentials(authorizationHeader);

        if (member == null) {
            return forbiddenStatus("Ungültige Anmeldeinformationen");
        }

        if ("AdminApp".equals(appId)) {
            if (member.isAdmin()) {
                return performOperation(entityManager -> {
                    return okResponse(getOpenCarts(entityManager));
                });
            } else {
                return forbiddenStatus("You need to be admin.");
            }
        } else if ("UserApp".equals(appId)) {
            return performOperation(entityManager -> {
                Object[] result = getOpenCarts(entityManager).stream()
                        .map(cart -> shoppingCartWithPersonalItemsOnly(cart, member))
                        .toArray();
                return okResponse(result);
            });
        }

        return serverErrorResponse("Invalid AppId");
    }

    public static List<ShoppingCart> getOpenCarts(EntityManager entityManager) {
        return entityManager.createQuery("Select c FROM ShoppingCart c WHERE c NOT IN (SELECT o.shoppingCart FROM Order o)").getResultList();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("Only admins can access this resource");
        }

        //TODO: Users should find this too..
        return super.findById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addItem(@HeaderParam("Authorization") String authorizationHeader,
            String itemToAddJson) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("Only admins can access this resource");
        }

        return super.add(itemToAddJson);
    }

    @DELETE
    @Path("/{id}")
    public Response removeItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("Only admins can access this resource");
        }

        return super.removeItem(id);
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id,
            String itemToUpdate) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("You need to be admin");
        }

        return super.updateItem(itemToUpdate);
    }

    private static ShoppingCart shoppingCartWithPersonalItemsOnly(ShoppingCart originalCart, Member user) {
        ShoppingCart filteredCart = new ShoppingCart();
        filteredCart.setId(originalCart.getId());

        List<ShoppingCartItem> filteredItems = originalCart.getItems().stream().filter(it -> it.getUser().getId() == user.getId()).collect(Collectors.toList());
        filteredCart.getItems().addAll(filteredItems);

        return filteredCart;
    }
}
