package de.dhbw.foodcoops.server.resources;

import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.ShoppingCart;
import de.dhbw.foodcoops.model.ShoppingCartItem;
import de.dhbw.foodcoops.server.AuthorizationUtils;
import static de.dhbw.foodcoops.server.ResponseFactory.*;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("members")
@Singleton
public class MemberResource extends BasicResource<Member> {

    public MemberResource() {
        super("Member", Member.class);

        boolean noAdminsExist = super.getAll()
                .stream()
                .filter(m -> m.isAdmin() && !m.isOutdated())
                .count() == 0;
        if (noAdminsExist) {
            Member firstMember = new Member();
            firstMember.setAdmin(true);
            firstMember.setUserName("admin");
            firstMember.setPassword("admin");
            firstMember.setFirstName("Admin");
            firstMember.setLastName("istrator");
            super.add(firstMember);
        }
    }

    @OPTIONS
    public Response preflightRequest_Collection() {
        return super.preflightRequest_Collection();
    }

    @OPTIONS
    @Path("/{id}")
    public Response preflightRequest_Resource() {
        return super.preflightRequest_Resource();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@HeaderParam("FC-App") String appId,
            @HeaderParam("Authorization") String authorizationHeader) {
        Member member = AuthorizationUtils.memberForCredentials(authorizationHeader);

        if (member == null) {
            return forbiddenStatus("Ungültige Anmeldeinformationen");
        }

        if ("AdminApp".equals(appId)) {
            if (member.isAdmin()) {
                List<Member> currentMembers = super.getAll().stream().filter(m -> !m.isOutdated()).collect(Collectors.toList());
                return okResponse(currentMembers);
            } else {
                return forbiddenStatus("You need to be admin.");
            }
        } else if ("UserApp".equals(appId)) {
            return okResponse(new Member[]{member});
        }

        return serverErrorResponse("Invalid AppId");
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("Only admins can access this resource");
        }

        return super.findById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response add(@HeaderParam("Authorization") String authorizationHeader,
            Member c) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("Only admin can access this resource");
        }

        return super.add(c);
    }

    @DELETE
    @Path("/{id}")
    public Response removeItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("Only admins can access this resource.");
        }

        try {
            return performOperation(entityManager -> {
                entityManager.getTransaction().begin();

                Member memberToRemove = entityManager.find(Member.class, id);
                memberToRemove.setOutdated(true);
                String randomId = UUID.randomUUID().toString();
                memberToRemove.setUserName(randomId);
                memberToRemove.setFirstName(randomId);
                memberToRemove.setLastName(randomId);
                memberToRemove.setMailAddress(randomId);
                memberToRemove.setPassword(randomId);
                entityManager.merge(memberToRemove);

                List<ShoppingCart> allOpenCarts = ShoppingCartResource.getOpenCarts(entityManager);
                for (ShoppingCart cart : allOpenCarts) {
                    List<ShoppingCartItem> cartItemsToRemove = cart.getItems().stream()
                            .filter(openCartItem -> id == openCartItem.getUser().getId())
                            .collect(Collectors.toList());

                    cartItemsToRemove.forEach(itemByDeletedUser -> {
                        cart.getItems().remove(itemByDeletedUser);
                        entityManager.merge(cart);
                        entityManager.remove(itemByDeletedUser);
                    });
                }

                entityManager.getTransaction().commit();
                return okResponse();
            });
        } catch (Exception ex) {
            return serverErrorResponse(ex.getMessage());
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id,
            String newValuesJson) {
        Member member = AuthorizationUtils.memberForCredentials(authorizationHeader);
        if (member == null
                || (!member.isAdmin() && member.getId() != id)) {
            return forbiddenStatus("Only admin or the manipulated member can access this resource");
        }

        return super.updateItem(newValuesJson);
    }
}
