package de.dhbw.foodcoops.server.resources;

import de.dhbw.foodcoops.model.Extraction;
import de.dhbw.foodcoops.model.ExtractionItem;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.WarehouseItem;
import de.dhbw.foodcoops.server.AuthorizationUtils;
import static de.dhbw.foodcoops.server.ResponseFactory.*;
import java.util.List;
import java.util.Optional;
import javax.persistence.Query;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("extractions")
public class ExtractionResource extends BasicResource<Extraction> {

    public ExtractionResource() {
        super("Extraction", Extraction.class);
    }

    @OPTIONS
    public Response preflightRequest_Collection() {
        System.out.println("Preflight SupplierCollection");
        return okResponse();
    }

    @OPTIONS
    @Path("/{id}")
    public Response preflightRequest_Resource() {
        System.out.println("Preflight suppliers/{id}");
        return okResponse();
    }
    
    @OPTIONS
    @Path("/{id}/payment")
    public Response preflightRequest_PaymentResource() {
        System.out.println("Preflight suppliers/{id}");
        return okResponse();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@HeaderParam("Authorization") String authorizationHeader) {
        Member member = AuthorizationUtils.memberForCredentials(authorizationHeader);

        if (member == null) {
            return forbiddenStatus("Ungültige Anmeldeinformationen");
        }

        return super.getAllResponse();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isMember(authorizationHeader)) {
            forbiddenStatus("Please provide valid credentials");
        }

        return super.findById(id);
    }

    @POST
    public Response addItem(@HeaderParam("Authorization") String authorizationHeader,
            String itemToAddJson) {
        if (!AuthorizationUtils.isMember(authorizationHeader)) {
            forbiddenStatus("Please provide valid credentials");
        }

        performOperation(entityManager -> {
            entityManager.getTransaction().begin();

            Extraction extraction = parseModel(itemToAddJson);
            entityManager.persist(extraction);

            Query query = entityManager.createQuery("Select c from WarehouseItem c");
            List<WarehouseItem> allWarehouseItems = query.getResultList();
            List<ExtractionItem> newWarehouseItems = extraction.getExtractionItems();
            for (ExtractionItem newItem : newWarehouseItems) {
                Optional<WarehouseItem> existingItemToUpdate = allWarehouseItems.stream()
                        .filter(i -> i.getId() == newItem.getWarehouseItem().getId())
                        .findFirst();

                if (existingItemToUpdate.isPresent()) {
                    int originalAmount = existingItemToUpdate.get().getAmount();

                    if (newItem.getAmount() <= originalAmount) {
                        existingItemToUpdate.get().setAmount(originalAmount - newItem.getAmount());
                    } else {
                        throw new IllegalStateException("Can't extract more than what's on stock");
                    }
                } else {
                    throw new IllegalStateException("No Warehouseitem found for exraction.");
                }
            }

            entityManager.getTransaction().commit();

            return null;
        });

        return okResponse();
    }

    @DELETE
    @Path("/{id}")
    public Response removeItem(@HeaderParam("FC-App") String appId,
            @HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        Member member = AuthorizationUtils.memberForCredentials(authorizationHeader);

        if (member == null) {
            return forbiddenStatus("Ungültige Anmeldeinformationen");
        }

        if ("AdminApp".equals(appId)) {
            if (member.isAdmin()) {
                return super.removeItem(id);
            } else {
                return forbiddenStatus("Diese Aktion steht nur Administratoren zur Verfügung");
            }
        } else if ("UserApp".equals(appId)) {
            return forbiddenStatus("Diese Aktion steht nur Administratoren zur Verfügung");
        }

        return serverErrorResponse("Invalid app Id");
    }

    @PUT
    @Path("/{id}")
    public Response updateItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id, String itemToUpdate) {
        return forbiddenStatus("You can't update this resource.");
    }

    @POST
    @Path("/{id}/payment")
    public Response payItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        return performOperation(entityManager -> {
            try {
                entityManager.getTransaction().begin();

                Extraction itemToUpdate = entityManager.find(Extraction.class, id);
                itemToUpdate.setPaid(true);

                entityManager.getTransaction().commit();
                return okResponse();
            } catch (Exception ex) {
                return serverErrorResponse();
            }
        });
    }
}
