function currentDateTimeString() {
    var time = currentDateString() + " - " + currentTimeString();
    return time;
}

function currentTimeString() {
    var objToday = new Date();

    var curHour = objToday.getHours();
    var curMinute = objToday.getMinutes() < 10 ? "0" + objToday.getMinutes() : objToday.getMinutes();
    var curSeconds = objToday.getSeconds() < 10 ? "0" + objToday.getSeconds() : objToday.getSeconds();
    
    var time = curHour + ":" + curMinute + ":" + curSeconds;
    return time;
}

function currentDateString() {
    var objToday = new Date();

    var curDay = objToday.getDate();
    var curMonth = objToday.getMonth() + 1;
    var curYear = objToday.getFullYear();
    
    var time = curYear + "/" + curMonth + "/" + curDay;
    return time;
}