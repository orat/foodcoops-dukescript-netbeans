package de.dhbw.foodcoops.js;

@FunctionalInterface
public interface ILogAction {
    void log(String message);
}
