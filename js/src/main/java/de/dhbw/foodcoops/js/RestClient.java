package de.dhbw.foodcoops.js;

import net.java.html.js.JavaScriptBody;
import net.java.html.js.JavaScriptResource;

@JavaScriptResource("rest-client.js")
public class RestClient implements IRestClient {
    private final IPlatformServices services;

    public RestClient(IPlatformServices services) {
        this.services = services;
    }
    
    @Override
    public void sendAsync(String resource, String method, String data, IRestCallback callback)
    {
        String username = services.getPreferences("authorization.user.name");
        String password = services.getPreferences("authorization.user.password");
        String appId = services.getPreferences("app.id");
        
        send(true, createUrl(resource), method, data, username, password, callback, appId);
    }
    
    @Override
    public void send(String resource, String method, String data, IRestCallback callback)
    {
        String username = services.getPreferences("authorization.user.name");
        String password = services.getPreferences("authorization.user.password");
        String appId = services.getPreferences("app.id");
        
        send(false, createUrl(resource), method, data, username, password, callback, appId);
    }
    
    private String createUrl(String resource)
    {
        String endpoint = services.getPreferences("api.endpoint");
        String sanitizedEndpoint = endpoint;
        if(!endpoint.endsWith("/")) {
            sanitizedEndpoint = endpoint + "/";
        }
        
        return sanitizedEndpoint + resource;
    }
    
    
    @JavaScriptBody(args = {"async" , "url", "method", "data", "username", "password", "callback", "appId"},
            javacall = true,
            body = "xhrSend(async, url, "
                    + "function(statusCode, responseText) {callback.@de.dhbw.foodcoops.js.IRestCallback::onSuccess(ILjava/lang/String;)(statusCode, responseText)},"
                    + "function(errorText) {callback.@de.dhbw.foodcoops.js.IRestCallback::onError(Ljava/lang/String;)(errorText)},"
                    + "method, data, "
                    + "username, password, "
                    + "appId);")
    private static native void send(boolean async, String url, String method, String data, String username, String password, IRestCallback callback, String appId);
}
