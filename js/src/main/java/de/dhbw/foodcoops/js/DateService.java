package de.dhbw.foodcoops.js;

import net.java.html.js.JavaScriptBody;
import net.java.html.js.JavaScriptResource;

@JavaScriptResource("date-service.js")
public class DateService {

    public String currentDateTimeString() {
        return currentDateTimeString_impl();
    }
    
    public String currentDateString() {
        return currentDateString_impl();
    }
    
    public String currentTimeString() {
        return currentTimeString_impl();
    }

    @JavaScriptBody(args = {},
            body = "return currentDateTimeString();")
    public static native String currentDateTimeString_impl();
    
    @JavaScriptBody(args = {},
            body = "return currentDateString();")
    public static native String currentDateString_impl();
    
    @JavaScriptBody(args = {},
            body = "return currentTimeString();")
    public static native String currentTimeString_impl();
}
