package de.dhbw.foodcoops.js;

import net.java.html.BrwsrCtx;
import net.java.html.js.JavaScriptBody;

public class PlatformServices implements IPlatformServices {
    private static final String DEFAULT_TOAST_DURATION = "1500";

    /**
     * Reads a value from a persistent storage.
     *
     * @param key the identification for the value
     * @return the value or <code>null</code> if not found
     */
    @Override
    public String getPreferences(String key) {
        return getPreferences_Impl(key);
    }

    /**
     * Reads a value from a persistent storage.
     *
     * @param key the identification for the value
     * @return the value or defaultValue if not found
     */
    @Override
    public String getPreferences(String key, String defaultValue) {
        String value = getPreferences(key);

        if (value == null) {
            return defaultValue;
        }

        return value;
    }

    @Override
    public void alert(String msg) {
        alert_Impl(msg);
    }

    /**
     * Puts a value into the persitent storage.
     *
     * @param key the identification for the value
     * @param value the value to store
     */
    @Override
    public void setPreferences(String key, String value) {
        setPreferences_Impl(key, value);
    }

    @Override
    public void setPreferencesIfNotExists(String key, String value) {
        if (getPreferences(key) == null) {
            setPreferences(key, value);
        }
    }

    /**
     * Shows confirmation dialog to the user.
     *
     * @param msg the message
     * @param callback called back when the use accepts (can be null)
     */
    @Override
    public void confirmByUser(String msg, Runnable callback) {
        BrwsrCtx.findDefault(PlatformServices.class)
                .execute(() -> confirm_Impl(msg, callback));
    }

    @JavaScriptBody(args = {"key"}, body
            = "if (!window.localStorage) return null;\n"
            + "return window.localStorage.getItem(key);\n"
    )
    private static native String getPreferences_Impl(String key);

    @JavaScriptBody(args = {"msg"}, body
            = "alert(msg);"
    )
    private static native String alert_Impl(String msg);

    @JavaScriptBody(args = {"key", "value"}, body
            = "if (!window.localStorage) return;\n"
            + "window.localStorage.setItem(key, value);\n"
    )
    private static native void setPreferences_Impl(String key, String value);

    /**
     * Shows confirmation dialog to the user.
     *
     * @param msg the message
     * @param callback called back when the use accepts (can be null)
     */
    @JavaScriptBody(
            args = {"msg", "callback"},
            javacall = true,
            body = "if (confirm(msg)) {\n"
            + "  callback.@java.lang.Runnable::run()();\n"
            + "}\n"
    )
    private static native void confirm_Impl(String msg, Runnable callback);

    @Override
    public void toastInfo(String msg) {
        String duration = getPreferences("app.toast.duration", DEFAULT_TOAST_DURATION);
        toastInfo_Impl(duration, msg);
    }

    @Override
    public void toastWarning(String msg) {
        String duration = getPreferences("app.toast.duration", DEFAULT_TOAST_DURATION);
        toastWarning_Impl(duration, msg);
    }

    @Override
    public void decorateSystemLogger(ILogAction standardLogCallback, ILogAction infoLogCallback, ILogAction warnLogCallback, ILogAction errorLogCallback) {
        decorateConsoleLogger(standardLogCallback, infoLogCallback, warnLogCallback, errorLogCallback);
    }

    @JavaScriptBody(args = {"standardLogCallback", "infoLogCallback", "warnLogCallback", "errorLogCallback"},
            javacall = true,
            body = "// define a new console\n"
            + "var console=(function(oldCons){\n"
            + "    return {\n"
            + "        log: function(text){\n"
            + "            oldCons.log(text);\n"
            + "            standardLogCallback.@de.dhbw.foodcoops.js.ILogAction::log(Ljava/lang/String;)(text);\n"
            + "        },\n"
            + "        info: function (text) {\n"
            + "            oldCons.info(text);\n"
            + "            infoLogCallback.@de.dhbw.foodcoops.js.ILogAction::log(Ljava/lang/String;)(text);\n"
            + "        },\n"
            + "        warn: function (text) {\n"
            + "            oldCons.warn(text);\n"
            + "            warnLogCallback.@de.dhbw.foodcoops.js.ILogAction::log(Ljava/lang/String;)(text);\n"
            + "        },\n"
            + "        error: function (text) {\n"
            + "            oldCons.error(text);\n"
            + "            errorLogCallback.@de.dhbw.foodcoops.js.ILogAction::log(Ljava/lang/String;)(text);\n"
            + "        }\n"
            + "    };\n"
            + "}(window.console));\n"
            + "\n"
            + "//Then redefine the old console\n"
            + "window.console = console;")
    private static native void decorateConsoleLogger(ILogAction standardLogCallback, ILogAction infoLogCallback, ILogAction warnLogCallback, ILogAction errorLogCallback);

    @JavaScriptBody(args = {"duration", "message"},
            body = "ons.notification.toast(message, { timeout: duration, animation: 'fall', class: 'infoToast' })")
    private static native void toastInfo_Impl(String duration, String message);

    @JavaScriptBody(args = {"duration", "message"},
            body = "ons.notification.toast(message, { timeout: duration, animation: 'fall', class: 'warningToast' })")
    private static native void toastWarning_Impl(String duration, String message);
}
