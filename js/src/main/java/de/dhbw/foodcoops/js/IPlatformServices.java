package de.dhbw.foodcoops.js;

public interface IPlatformServices {
    public void alert(String msg);
    
    public void toastInfo(String msg);
    public void toastWarning(String msg);

    /**
     * Reads a value from a persistent storage.
     *
     * @param key the identification for the value
     * @return the value or <code>null</code> if not found
     */
    public String getPreferences(String key);
    
    /**
     * Reads a value from a persistent storage.
     *
     * @param key the identification for the value
     * @param defaultValue the value that is returned, if no value was found in the preferences
     * @return the value or defaultValue if not found
     */
    public String getPreferences(String key, String defaultValue);
    
    /**
     * Puts a value into the persitent storage.
     *
     * @param key the identification for the value
     * @param value the value to store
     */
    public void setPreferences(String key, String value);
    /**
     * Puts a value into the persitent storage.
     *
     * @param key the identification for the value
     * @param value the value to store
     */
    public void setPreferencesIfNotExists(String key, String value);

    /**
     * Shows confirmation dialog to the user.
     *
     * @param msg the message
     * @param callback called back when the use accepts (can be null)
     */
    public void confirmByUser(String msg, Runnable callback);
    
    public void decorateSystemLogger(ILogAction standardLogCallback, ILogAction infoLogCallback, ILogAction warnLogCallback, ILogAction errorLogCallback);
}
