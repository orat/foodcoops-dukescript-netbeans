
# Reference for mvn project dependency builds: https://books.sonatype.com/mvnref-book/reference/_using_advanced_reactor_options.html#_specifying_a_subset_of_projects

alias fc-run-web-user="mvn clean install -pl client-web -am -Dapp=userApp && mvn package bck2brwsr:show -pl client-web -Dapp=userApp"
alias fc-run-web-admin="mvn clean install -pl client-web -am -Dapp=adminApp && mvn package bck2brwsr:show -pl client-web -Dapp=adminApp"

alias fc-run-android-user="mvn clean install -pl client-android -am -Dapp=userApp && mvn package android:deploy android:run -pl client-android -Dapp=userApp"
alias fc-run-android-admin="mvn clean install -pl client-android -am -Dapp=adminApp && mvn package android:deploy android:run -pl client-android -Dapp=adminApp"

alias fc-run-server="mvn clean install -pl server -am -PskipTests && mvn tomcat7:run -pl server -PskipTests"

alias fc-clean-and-build="mvn clean install -PskipTests"
alias fc-remove-maven-artifacts="removeMavenArtifacts 'de/dhbw/foodcoops'"




function removeMavenArtifacts() {
    if [ -z "$1" ]
    then
        echo "No argument supplied"
        return 1
    fi
    
    local maven_repository_home="~/.m2/repository"
    local artifact_root_dir="$maven_repository_home/$1"
    
    echo "Attempt to remove: $artifact_root_dir"
    
    
    [ ! -d "`eval echo ${maven_repository_home//>}`" ] && echo "Maven repository home does not exist: '$maven_repository_home'" && return 2
    #[ ! -d "`eval echo ${artifact_root_dir//>}`" ] && echo "Artifact folder does not exist" && return 3 
    
    
    rm -r "$artifact_root_dir" && echo "Successfully removed"
}
